import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:diacritic/diacritic.dart';
import 'package:responsive_container/responsive_container.dart';
import 'package:auto_size_text/auto_size_text.dart';

void main() {
  runApp(MaterialApp(
    home: Home(),
  ));
}

String jsonData = "[{ " +
    " \"name\" : \"Semente de Girassol sem Casca\"," +
    " \"info_about\" : \"A semente de girassol é facilmente encontrada com casca, sem casca, crua ou torrada. Seu custo é baixo, seu sabor delicado, suas propriedades riquíssimas\"," +
    " \"standard_portion\" : \"10.0\", " +
    " \"energy_value_a\" : \"60.0\", " +
    " \"energy_value_dv\" : \"3.0\"," +
    " \"energy_value_un\" : \"kcal\"," +
    " \"carbohydrates_a\" : \"1.7\"," +
    " \"carbohydrates_dv\" : \"1.0\"," +
    " \"carbohydrates_un\" : \"g\"," +
    " \"sugar_a\" : \"-\"," +
    " \"sugar_dv\" : \"-\"," +
    " \"sugar_un\" : \"-\"," +
    " \"protein_a\" : \"2.0\"," +
    " \"protein_dv\" : \"0\"," +
    " \"protein_un\" : \"g\"," +
    " \"trans_fat_a\" : \"0\"," +
    " \"trans_fat_dv\" : \"0\"," +
    " \"trans_fat_un\" : \"g\"," +
    " \"sat_fat_a\" : \"0\"," +
    " \"sat_fat_dv\" : \"0\"," +
    " \"sat_fat_un\" : \"g\"," +
    " \"mnuns_fat_a\" : \"1.0\"," +
    " \"mnuns_fat_dv\" : \"-\"," +
    " \"mnuns_fat_un\" : \"g\"," +
    " \"pluns_fat_a\" : \"3.3\", " +
    " \"pluns_fat_dv\" : \"-\"," +
    " \"pluns_fat_un\" : \"g\"," +
    " \"fibers_a\" : \"3.0\"," +
    " \"fibers_dv\" : \"12.0\"," +
    " \"fibers_un\" : \"g\"," +
    " \"sucrose_a\" : \"0\", " +
    " \"sucrose_dv\" : \"0\"," +
    " \"sucrose_un\" : \"g\"," +
    " \"cholesterol_a\" : \"0\"," +
    " \"cholesterol_dv\" : \"-\"," +
    " \"cholesterol_un\" : \"mg\"," +
    " \"iron_a\" : \"0.6\"," +
    " \"iron_dv\" : \"4.0\"," +
    " \"iron_un\" : \"mg\"," +
    " \"zync_a\" : \"-\"," +
    " \"zync_dv\" : \"-\"," +
    " \"zync_un\" : \"-\"," +
    " \"calcium_a\" : \"-\"," +
    " \"calcium_dv\" : \"-\"," +
    " \"calcium_un\" : \"-\"," +
    " \"vitamin_a_a\" : \"-\"," +
    " \"vitamin_a_dv\" : \"-\"," +
    " \"vitamin_a_un\" : \"-\"," +
    " \"vitamin_c_a\" : \"-\"," +
    " \"vitamin_c_dv\" : \"-\"," +
    " \"vitamin_c_un\" : \"-\"," +
    " \"vitamin_d_a\" : \"-\"," +
    " \"vitamin_d_dv\" : \"-\"," +
    " \"vitamin_d_un\" : \"-\"," +
    " \"vitamin_e_a\" : \"-\"," +
    " \"vitamin_e_dv\" : \"-\"," +
    " \"vitamin_e_un\" : \"-\"," +
    " \"vitamin_b1_a\" : \"-\"," +
    " \"vitamin_b1_dv\" : \"-\"," +
    " \"vitamin_b1_un\" : \"-\"," +
    " \"vitamin_b3_a\" : \"-\"," +
    " \"vitamin_b3_dv\" : \"-\"," +
    " \"vitamin_b3_un\" : \"-\"," +
    " \"vitamin_b6_a\" : \"-\"," +
    " \"vitamin_b6_dv\" : \"-\"," +
    " \"vitamin_b6_un\" : \"-\"," +
    " \"vitamin_b9_a\" : \"-\"," +
    " \"vitamin_b9_dv\" : \"-\"," +
    " \"vitamin_b9_un\" : \"-\"," +
    " \"vitamin_b12_a\" : \"-\"," +
    " \"vitamin_b12_dv\" : \"-\"," +
    " \"vitamin_b12_un\" : \"-\"}," +
    " { " +
    " \"name\" : \"Abacate Cru\"," +
    " \"info_about\" : \"Os abacates, assim como outras frutas e vegetais, contém vitaminas A e do complexo B e alguns sais minerais como ferro, cálcio e o fósforo. Eles são ricos em vitaminas E e C, potentes antioxidantes que ajudam a promover a saúde dos dentes e das gengivas, e também protegem os tecidos do corpo de danos oxidativos. Além disso, a presença de folatos (vitamina do complexo B) no abacate promove o desenvolvimento saudável das células e dos tecidos\"," +
    " \"standard_portion\" : \"100.0\", " +
    " \"energy_value_a\" : \"97.2\", " +
    " \"energy_value_dv\" : \"2.0\"," +
    " \"energy_value_un\" : \"kcal\"," +
    " \"carbohydrates_a\" : \"6.0\"," +
    " \"carbohydrates_dv\" : \"1.0\"," +
    " \"carbohydrates_un\" : \"g\"," +
    " \"sugar_a\" : \"-\"," +
    " \"sugar_dv\" : \"-\"," +
    " \"sugar_un\" : \"-\"," +
    " \"protein_a\" : \"2.0\"," +
    " \"protein_dv\" : \"0\"," +
    " \"protein_un\" : \"g\"," +
    " \"trans_fat_a\" : \"0\"," +
    " \"trans_fat_dv\" : \"0\"," +
    " \"trans_fat_un\" : \"g\"," +
    " \"sat_fat_a\" : \"0\"," +
    " \"sat_fat_dv\" : \"0\"," +
    " \"sat_fat_un\" : \"g\"," +
    " \"mnuns_fat_a\" : \"1.0\"," +
    " \"mnuns_fat_dv\" : \"-\"," +
    " \"mnuns_fat_un\" : \"g\"," +
    " \"pluns_fat_a\" : \"3.3\", " +
    " \"pluns_fat_dv\" : \"-\"," +
    " \"pluns_fat_un\" : \"g\"," +
    " \"fibers_a\" : \"3.0\"," +
    " \"fibers_dv\" : \"12.0\"," +
    " \"fibers_un\" : \"g\"," +
    " \"sucrose_a\" : \"0\", " +
    " \"sucrose_dv\" : \"0\"," +
    " \"sucrose_un\" : \"g\"," +
    " \"cholesterol_a\" : \"0\"," +
    " \"cholesterol_dv\" : \"-\"," +
    " \"cholesterol_un\" : \"mg\"," +
    " \"iron_a\" : \"0.6\"," +
    " \"iron_dv\" : \"4.0\"," +
    " \"iron_un\" : \"mg\"," +
    " \"zync_a\" : \"-\"," +
    " \"zync_dv\" : \"-\"," +
    " \"zync_un\" : \"-\"," +
    " \"calcium_a\" : \"-\"," +
    " \"calcium_dv\" : \"-\"," +
    " \"calcium_un\" : \"-\"," +
    " \"vitamin_a_a\" : \"-\"," +
    " \"vitamin_a_dv\" : \"-\"," +
    " \"vitamin_a_un\" : \"-\"," +
    " \"vitamin_c_a\" : \"-\"," +
    " \"vitamin_c_dv\" : \"-\"," +
    " \"vitamin_c_un\" : \"-\"," +
    " \"vitamin_d_a\" : \"-\"," +
    " \"vitamin_d_dv\" : \"-\"," +
    " \"vitamin_d_un\" : \"-\"," +
    " \"vitamin_e_a\" : \"-\"," +
    " \"vitamin_e_dv\" : \"-\"," +
    " \"vitamin_e_un\" : \"-\"," +
    " \"vitamin_b1_a\" : \"-\"," +
    " \"vitamin_b1_dv\" : \"-\"," +
    " \"vitamin_b1_un\" : \"-\"," +
    " \"vitamin_b3_a\" : \"-\"," +
    " \"vitamin_b3_dv\" : \"-\"," +
    " \"vitamin_b3_un\" : \"-\"," +
    " \"vitamin_b6_a\" : \"-\"," +
    " \"vitamin_b6_dv\" : \"-\"," +
    " \"vitamin_b6_un\" : \"-\"," +
    " \"vitamin_b9_a\" : \"-\"," +
    " \"vitamin_b9_dv\" : \"-\"," +
    " \"vitamin_b9_un\" : \"-\"," +
    " \"vitamin_b12_a\" : \"-\"," +
    " \"vitamin_b12_dv\" : \"-\"," +
    " \"vitamin_b12_un\" : \"-\"},"
        " { " +
    "\"name\" : \"Salmão cru\"," +
    "\"info_about\" : \"Salmão é o nome vulgar de várias espécies de peixes da família Salmonidae, que também inclui as trutas, típicos das águas frias do norte da Eurásia e da América. Várias espécies são criadas em aquacultura, especialmente a espécie Salmo salar\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"170.0\", " +
    "\"energy_value_dv\" : \"8.5\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"0\"," +
    "\"carbohydrates_dv\" : \"0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"19.3\"," +
    "\"protein_dv\" : \"38.6\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"9.0\"," +
    "\"sat_fat_dv\" : \"2.0\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0\"," +
    "\"mnuns_fat_dv\" : \"0\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0\", " +
    "\"pluns_fat_dv\" : \"0\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"0\"," +
    "\"fibers_dv\" : \"0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"53.0\"," +
    "\"cholesterol_dv\" : \"17.6\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"1.3\"," +
    "\"iron_dv\" : \"13.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"3.2\"," +
    "\"zync_dv\" : \"21.3\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"9.0\"," +
    "\"calcium_dv\" : \"1.1\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"mg\"," +
    "\"vitamin_c_a\" : \"-\"," +
    "\"vitamin_c_dv\" : \"-\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"mg\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"11.3\"," +
    "\"vitamin_b3_dv\" : \"5.3\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.52\"," +
    "\"vitamin_b6_dv\" : \"0.1\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"3.25\"," +
    "\"vitamin_b12_dv\" : \"0.8\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "},{" +
    "\"name\" : \"Salame\"," +
    "\"info_about\" : \"Um salame é um enchido de origem italiana. O nome é derivado do verbo italiano salare, que significa salgar\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"398.0\", " +
    "\"energy_value_dv\" : \"19.9\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"2.9\"," +
    "\"carbohydrates_dv\" : \"0.9\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"25.8\"," +
    "\"protein_dv\" : \"10.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"9.0\"," +
    "\"sat_fat_dv\" : \"48.0\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"11.0\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"2.5\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"0\"," +
    "\"fibers_dv\" : \"0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"85.0\"," +
    "\"cholesterol_dv\" : \"28.3\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"1.3\"," +
    "\"iron_dv\" : \"13.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"3.2\"," +
    "\"zync_dv\" : \"21.3\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"87.0\"," +
    "\"calcium_dv\" : \"10.8\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"0\"," +
    "\"vitamin_c_dv\" : \"0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.5\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{" +
    "\"name\" : \"Sardinha crua\"," +
    "\"info_about\" : \" As sardinhas ou manjuas são peixes da família Clupeidae, aparentados com os arenques. Geralmente de pequenas dimensões, caracterizam-se por possuírem apenas uma barbatana dorsal sem espinhos\",\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"114.0\"," +
    "\"energy_value_dv\" : \"5.7\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"0\"," +
    "\"carbohydrates_dv\" : \"0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"\"," +
    "\"protein_a\" : \"21.1\"," +
    "\"protein_dv\" : \"42.2\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"1.0\"," +
    "\"sat_fat_dv\" : \"8.5\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"3.9\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"5\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"0\"," +
    "\"fibers_dv\" : \"0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"61.0\"," +
    "\"cholesterol_dv\" : \"4.1\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"1.3\"," +
    "\"iron_dv\" : \"13.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"1.3\"," +
    "\"zync_dv\" : \"8.6\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"167.0\"," +
    "\"calcium_dv\" : \"20.8\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"108\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"-\"," +
    "\"vitamin_c_dv\" : \"-\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"197.0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.6\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"}," +
    "{" +
    "\"name\" : \"Shoyu\"," +
    "\"info_about\" : \"O molho de soja é um molho fabricado a partir de uma mistura de soja, cereal torrado, água e sal marinho, fermentando-os com microrganismos Aspergillus oryzae e/ou Aspergillus sojae.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"61.0\", " +
    "\"energy_value_dv\" : \"3.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"11.6\"," +
    "\"carbohydrates_dv\" : \"3.8\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0.4\"," +
    "\"sugar_dv\" : \"0.2\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"3.3\"," +
    "\"protein_dv\" : \"6.6\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0.1\"," +
    "\"sat_fat_dv\" : \"0.04\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0.3\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"5\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"0\"," +
    "\"fibers_dv\" : \"0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.5\"," +
    "\"iron_dv\" : \"5.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.2\"," +
    "\"zync_dv\" : \"1.3\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"15.0\"," +
    "\"calcium_dv\" : \"1.8\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"-\"," +
    "\"vitamin_c_dv\" : \"-\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"}," +
    "{ \"name\" : \"Maracujá Cru\"," +
    " \"info_about\" : \"Maracujá é um fruto produzido pelas plantas do gênero Passiflora. Pertencente à família Passifloraceae, o maracujazeiro é originário da América Tropical e possui mais de 150 espécies. As espécies mais cultivadas são maracujá-amarelo, maracujá-roxo e o maracujá-doce.\"," +
    " \"standard_portion\" : \"100.0\", " +
    " \"energy_value_a\" : \"68.4\", " +
    " \"energy_value_dv\" : \"3.0\"," +
    " \"energy_value_un\" : \"kcal\"," +
    " \"carbohydrates_a\" : \"12.3\"," +
    " \"carbohydrates_dv\" : \"4.0\"," +
    " \"carbohydrates_un\" : \"g\"," +
    " \"sugar_a\" : \"-\"," +
    " \"sugar_dv\" : \"-\"," +
    " \"sugar_un\" : \"-\"," +
    " \"protein_a\" : \"2.0\"," +
    " \"protein_dv\" : \"3.0\"," +
    " \"protein_un\" : \"g\"," +
    " \"trans_fat_a\" : \"0\"," +
    " \"trans_fat_dv\" : \"0\"," +
    " \"trans_fat_un\" : \"g\"," +
    " \"sat_fat_a\" : \"0.2\"," +
    " \"sat_fat_dv\" : \"1.0\"," +
    " \"sat_fat_un\" : \"g\"," +
    " \"mnuns_fat_a\" : \"0.9\"," +
    " \"mnuns_fat_dv\" : \"-\"," +
    " \"mnuns_fat_un\" : \"g\"," +
    " \"pluns_fat_a\" : \"0.9\", " +
    " \"pluns_fat_dv\" : \"-\"," +
    " \"pluns_fat_un\" : \"g\"," +
    " \"fibers_a\" : \"1.1\"," +
    " \"fibers_dv\" : \"4.0\"," +
    " \"fibers_un\" : \"g\"," +
    " \"sucrose_a\" : \"-\", " +
    " \"sucrose_dv\" : \"-\"," +
    " \"sucrose_un\" : \"-\"," +
    " \"cholesterol_a\" : \"-\"," +
    " \"cholesterol_dv\" : \"-\"," +
    " \"cholesterol_un\" : \"-\"," +
    " \"iron_a\" : \"0.6\"," +
    " \"iron_dv\" : \"4.0\"," +
    " \"iron_un\" : \"mg\"," +
    " \"zync_a\" : \"0.4\"," +
    " \"zync_dv\" : \"6.0\"," +
    " \"zync_un\" : \"mg\"," +
    " \"calcium_a\" : \"5.4\"," +
    " \"calcium_dv\" : \"1.0\"," +
    " \"calcium_un\" : \"mg\"," +
    " \"vitamin_a_a\" : \"-\"," +
    " \"vitamin_a_dv\" : \"-\"," +
    " \"vitamin_a_un\" : \"-\"," +
    " \"vitamin_c_a\" : \"19.8\"," +
    " \"vitamin_c_dv\" : \"44.0\"," +
    " \"vitamin_c_un\" : \"mg\"," +
    " \"vitamin_d_a\" : \"-\"," +
    " \"vitamin_d_dv\" : \"-\"," +
    " \"vitamin_d_un\" : \"-\"," +
    " \"vitamin_e_a\" : \"-\"," +
    " \"vitamin_e_dv\" : \"-\"," +
    " \"vitamin_e_un\" : \"-\"," +
    " \"vitamin_b1_a\" : \"-\"," +
    " \"vitamin_b1_dv\" : \"-\"," +
    " \"vitamin_b1_un\" : \"-\"," +
    " \"vitamin_b3_a\" : \"-\"," +
    " \"vitamin_b3_dv\" : \"-\"," +
    " \"vitamin_b3_un\" : \"-\"," +
    " \"vitamin_b6_a\" : \"0.1\"," +
    " \"vitamin_b6_dv\" : \"8.0\"," +
    " \"vitamin_b6_un\" : \"mg\"," +
    " \"vitamin_b9_a\" : \"-\"," +
    " \"vitamin_b9_dv\" : \"-\"," +
    " \"vitamin_b9_un\" : \"-\"," +
    " \"vitamin_b12_a\" : \"-\"," +
    " \"vitamin_b12_dv\" : \"-\"," +
    " \"vitamin_b12_un\" : \"-\"}," +
    "{ \"name\" : \"Nabo Cru\"," +
    " \"info_about\" : \"Nabo é ótimo para a saúde do coração. Embora seja rico em nutrientes de uma maneira geral, um que recebe destaque é o potássio.\"," +
    " \"standard_portion\" : \"100.0\", " +
    " \"energy_value_a\" : \"18.2\", " +
    " \"energy_value_dv\" : \"1.0\"," +
    " \"energy_value_un\" : \"kcal\"," +
    " \"carbohydrates_a\" : \"4.2\"," +
    " \"carbohydrates_dv\" : \"1.0\"," +
    " \"carbohydrates_un\" : \"g\"," +
    " \"sugar_a\" : \"-\"," +
    " \"sugar_dv\" : \"-\"," +
    " \"sugar_un\" : \"-\"," +
    " \"protein_a\" : \"1.2\"," +
    " \"protein_dv\" : \"2.0\"," +
    " \"protein_un\" : \"g\"," +
    " \"trans_fat_a\" : \"-\"," +
    " \"trans_fat_dv\" : \"-\"," +
    " \"trans_fat_un\" : \"-\"," +
    " \"sat_fat_a\" : \"-\"," +
    " \"sat_fat_dv\" : \"-\"," +
    " \"sat_fat_un\" : \"-\"," +
    " \"mnuns_fat_a\" : \"-\"," +
    " \"mnuns_fat_dv\" : \"-\"," +
    " \"mnuns_fat_un\" : \"-\"," +
    " \"pluns_fat_a\" : \"-\", " +
    " \"pluns_fat_dv\" : \"-\"," +
    " \"pluns_fat_un\" : \"-\"," +
    " \"fibers_a\" : \"-\"," +
    " \"fibers_dv\" : \"-\"," +
    " \"fibers_un\" : \"-\"," +
    " \"sucrose_a\" : \"-\", " +
    " \"sucrose_dv\" : \"-\"," +
    " \"sucrose_un\" : \"-\"," +
    " \"cholesterol_a\" : \"-\"," +
    " \"cholesterol_dv\" : \"-\"," +
    " \"cholesterol_un\" : \"-\"," +
    " \"iron_a\" : \"0.2\"," +
    " \"iron_dv\" : \"1.0\"," +
    " \"iron_un\" : \"mg\"," +
    " \"zync_a\" : \"0.2\"," +
    " \"zync_dv\" : \"3.0\"," +
    " \"zync_un\" : \"mg\"," +
    " \"calcium_a\" : \"-\"," +
    " \"calcium_dv\" : \"-\"," +
    " \"calcium_un\" : \"-\"," +
    " \"vitamin_a_a\" : \"-\"," +
    " \"vitamin_a_dv\" : \"-\"," +
    " \"vitamin_a_un\" : \"-\"," +
    " \"vitamin_c_a\" : \"9.6\"," +
    " \"vitamin_c_dv\" : \"21.0\"," +
    " \"vitamin_c_un\" : \"mg\"," +
    " \"vitamin_d_a\" : \"-\"," +
    " \"vitamin_d_dv\" : \"-\"," +
    " \"vitamin_d_un\" : \"-\"," +
    " \"vitamin_e_a\" : \"-\"," +
    " \"vitamin_e_dv\" : \"-\"," +
    " \"vitamin_e_un\" : \"-\"," +
    " \"vitamin_b1_a\" : \"0.1\"," +
    " \"vitamin_b1_dv\" : \"7.0\"," +
    " \"vitamin_b1_un\" : \"mg\"," +
    " \"vitamin_b3_a\" : \"-\"," +
    " \"vitamin_b3_dv\" : \"-\"," +
    " \"vitamin_b3_un\" : \"-\"," +
    " \"vitamin_b6_a\" : \"0\"," +
    " \"vitamin_b6_dv\" : \"0\"," +
    " \"vitamin_b6_un\" : \"mg\"," +
    " \"vitamin_b9_a\" : \"-\"," +
    " \"vitamin_b9_dv\" : \"-\"," +
    " \"vitamin_b9_un\" : \"-\"," +
    " \"vitamin_b12_a\" : \"-\"," +
    " \"vitamin_b12_dv\" : \"-\"," +
    " \"vitamin_b12_un\" : \"-\"}," +
    "{ \"name\" : \"Ovo de galinha inteiro cru\"," +
    " \"info_about\" : \"O ovo, um dos alimentos mais completos em termos nutricionais, já foi vilão e recomendado a ser ingerido com moderação, atualmente, com diversos estudos, foi elevado ao patamar de alimento festejado por praticantes de atividades físicas e ‘cultuado’ no mundo fitness. Parte desse culto deve-se a sua diversidade de nutrientes e por ser rico em proteína.\"," +
    " \"standard_portion\" : \"100.0\", " +
    " \"energy_value_a\" : \"143.1\", " +
    " \"energy_value_dv\" : \"7.0\"," +
    " \"energy_value_un\" : \"kcal\"," +
    " \"carbohydrates_a\" : \"1.6\"," +
    " \"carbohydrates_dv\" : \"1.0\"," +
    " \"carbohydrates_un\" : \"g\"," +
    " \"sugar_a\" : \"-\"," +
    " \"sugar_dv\" : \"-\"," +
    " \"sugar_un\" : \"-\"," +
    " \"protein_a\" : \"-\"," +
    " \"protein_dv\" : \"-\"," +
    " \"protein_un\" : \"-\"," +
    " \"trans_fat_a\" : \"-\"," +
    " \"trans_fat_dv\" : \"-\"," +
    " \"trans_fat_un\" : \"-\"," +
    " \"sat_fat_a\" : \"-\"," +
    " \"sat_fat_dv\" : \"-\"," +
    " \"sat_fat_un\" : \"-\"," +
    " \"mnuns_fat_a\" : \"-\"," +
    " \"mnuns_fat_dv\" : \"-\"," +
    " \"mnuns_fat_un\" : \"-\"," +
    " \"pluns_fat_a\" : \"-\", " +
    " \"pluns_fat_dv\" : \"-\"," +
    " \"pluns_fat_un\" : \"-\"," +
    " \"fibers_a\" : \"-\"," +
    " \"fibers_dv\" : \"-\"," +
    " \"fibers_un\" : \"-\"," +
    " \"sucrose_a\" : \"-\", " +
    " \"sucrose_dv\" : \"-\"," +
    " \"sucrose_un\" : \"-\"," +
    " \"cholesterol_a\" : \"355.9\"," +
    " \"cholesterol_dv\" : \"-\"," +
    " \"cholesterol_un\" : \"mg\"," +
    " \"iron_a\" : \"1.6\"," +
    " \"iron_dv\" : \"11.0\"," +
    " \"iron_un\" : \"mg\"," +
    " \"zync_a\" : \"1.1\"," +
    " \"zync_dv\" : \"16.0\"," +
    " \"zync_un\" : \"mg\"," +
    " \"calcium_a\" : \"42.0\"," +
    " \"calcium_dv\" : \"4.0\"," +
    " \"calcium_un\" : \"mg\"," +
    " \"vitamin_a_a\" : \"78.8\"," +
    " \"vitamin_a_dv\" : \"13.0\"," +
    " \"vitamin_a_un\" : \"ug\"," +
    " \"vitamin_c_a\" : \"-\"," +
    " \"vitamin_c_dv\" : \"-\"," +
    " \"vitamin_c_un\" : \"-\"," +
    " \"vitamin_d_a\" : \"-\"," +
    " \"vitamin_d_dv\" : \"-\"," +
    " \"vitamin_d_un\" : \"-\"," +
    " \"vitamin_e_a\" : \"-\"," +
    " \"vitamin_e_dv\" : \"-\"," +
    " \"vitamin_e_un\" : \"-\"," +
    " \"vitamin_b1_a\" : \"0.1\"," +
    " \"vitamin_b1_dv\" : \"7.0\"," +
    " \"vitamin_b1_un\" : \"mg\"," +
    " \"vitamin_b3_a\" : \"-\"," +
    " \"vitamin_b3_dv\" : \"-\"," +
    " \"vitamin_b3_un\" : \"-\"," +
    " \"vitamin_b6_a\" : \"-\"," +
    " \"vitamin_b6_dv\" : \"-\"," +
    " \"vitamin_b6_un\" : \"-\"," +
    " \"vitamin_b9_a\" : \"-\"," +
    " \"vitamin_b9_dv\" : \"-\"," +
    " \"vitamin_b9_un\" : \"-\"," +
    " \"vitamin_b12_a\" : \"-\"," +
    " \"vitamin_b12_dv\" : \"-\"," +
    " \"vitamin_b12_un\" : \"-\"},"
        "{ \"name\" : \"Pequi Cru\"," +
    " \"info_about\" : \"O pequi (Caryocar brasiliense), também chamado de pequizeiro,[1] piqui, piquiá e pequiá, é uma árvore da família das cariocaráceas nativa do cerrado brasileiro. Seu fruto é muito utilizado na culinária sertaneja.\"," +
    " \"standard_portion\" : \"100.0\", " +
    " \"energy_value_a\" : \"205.0\", " +
    " \"energy_value_dv\" : \"10.0\"," +
    " \"energy_value_un\" : \"kcal\"," +
    " \"carbohydrates_a\" : \"13.0\"," +
    " \"carbohydrates_dv\" : \"4.0\"," +
    " \"carbohydrates_un\" : \"g\"," +
    " \"sugar_a\" : \"-\"," +
    " \"sugar_dv\" : \"-\"," +
    " \"sugar_un\" : \"-\"," +
    " \"protein_a\" : \"19.0\"," +
    " \"protein_dv\" : \"3.0\"," +
    " \"protein_un\" : \"g\"," +
    " \"trans_fat_a\" : \"-\"," +
    " \"trans_fat_dv\" : \"-\"," +
    " \"trans_fat_un\" : \"-\"," +
    " \"sat_fat_a\" : \"-\"," +
    " \"sat_fat_dv\" : \"-\"," +
    " \"sat_fat_un\" : \"-\"," +
    " \"mnuns_fat_a\" : \"-\"," +
    " \"mnuns_fat_dv\" : \"-\"," +
    " \"mnuns_fat_un\" : \"-\"," +
    " \"pluns_fat_a\" : \"-\", " +
    " \"pluns_fat_dv\" : \"-\"," +
    " \"pluns_fat_un\" : \"-\"," +
    " \"fibers_a\" : \"19.0\"," +
    " \"fibers_dv\" : \"76.0\"," +
    " \"fibers_un\" : \"g\"," +
    " \"sucrose_a\" : \"-\", " +
    " \"sucrose_dv\" : \"-\"," +
    " \"sucrose_un\" : \"-\"," +
    " \"cholesterol_a\" : \"-\"," +
    " \"cholesterol_dv\" : \"-\"," +
    " \"cholesterol_un\" : \"-\"," +
    " \"iron_a\" : \"0.3\"," +
    " \"iron_dv\" : \"2.0\"," +
    " \"iron_un\" : \"mg\"," +
    " \"zync_a\" : \"1.0\"," +
    " \"zync_dv\" : \"14.0\"," +
    " \"zync_un\" : \"mg\"," +
    " \"calcium_a\" : \"32.4\"," +
    " \"calcium_dv\" : \"3.0\"," +
    " \"calcium_un\" : \"mg\"," +
    " \"vitamin_a_a\" : \"-\"," +
    " \"vitamin_a_dv\" : \"-\"," +
    " \"vitamin_a_un\" : \"-\"," +
    " \"vitamin_c_a\" : \"8.3\"," +
    " \"vitamin_c_dv\" : \"3.0\"," +
    " \"vitamin_c_un\" : \"mg\"," +
    " \"vitamin_d_a\" : \"-\"," +
    " \"vitamin_d_dv\" : \"-\"," +
    " \"vitamin_d_un\" : \"-\"," +
    " \"vitamin_e_a\" : \"-\"," +
    " \"vitamin_e_dv\" : \"-\"," +
    " \"vitamin_e_un\" : \"-\"," +
    " \"vitamin_b1_a\" : \"0.2\"," +
    " \"vitamin_b1_dv\" : \"14.0\"," +
    " \"vitamin_b1_un\" : \"mg\"," +
    " \"vitamin_b3_a\" : \"-\"," +
    " \"vitamin_b3_dv\" : \"-\"," +
    " \"vitamin_b3_un\" : \"-\"," +
    " \"vitamin_b6_a\" : \"0.1\"," +
    " \"vitamin_b6_dv\" : \"8.0\"," +
    " \"vitamin_b6_un\" : \"mg\"," +
    " \"vitamin_b9_a\" : \"-\"," +
    " \"vitamin_b9_dv\" : \"-\"," +
    " \"vitamin_b9_un\" : \"-\"," +
    " \"vitamin_b12_a\" : \"-\"," +
    " \"vitamin_b12_dv\" : \"-\"," +
    " \"vitamin_b12_un\" : \"-\"}," +
    "{ \"name\" : \"Quiabo Cru\"," +
    " \"info_about\" : \"O quiabo é um fruto muito conhecido na culinária típica e apresenta grande quantidade de vitaminas e cálcio. O quiabeiro (Abelmoschus esculentus) é uma planta da família Malvaceae, que provavelmente foi introduzida no Brasil pelos escravos.\"," +
    " \"standard_portion\" : \"100.0\", " +
    " \"energy_value_a\" : \"29.9\", " +
    " \"energy_value_dv\" : \"2.0\"," +
    " \"energy_value_un\" : \"kcal\"," +
    " \"carbohydrates_a\" : \"6.4\"," +
    " \"carbohydrates_dv\" : \"3.0\"," +
    " \"carbohydrates_un\" : \"g\"," +
    " \"sugar_a\" : \"-\"," +
    " \"sugar_dv\" : \"-\"," +
    " \"sugar_un\" : \"-\"," +
    " \"protein_a\" : \"1.9\"," +
    " \"protein_dv\" : \"3.0\"," +
    " \"protein_un\" : \"g\"," +
    " \"trans_fat_a\" : \"-\"," +
    " \"trans_fat_dv\" : \"-\"," +
    " \"trans_fat_un\" : \"-\"," +
    " \"sat_fat_a\" : \"-\"," +
    " \"sat_fat_dv\" : \"-\"," +
    " \"sat_fat_un\" : \"-\"," +
    " \"mnuns_fat_a\" : \"-\"," +
    " \"mnuns_fat_dv\" : \"-\"," +
    " \"mnuns_fat_un\" : \"-\"," +
    " \"pluns_fat_a\" : \"-\", " +
    " \"pluns_fat_dv\" : \"-\"," +
    " \"pluns_fat_un\" : \"-\"," +
    " \"fibers_a\" : \"4.6\"," +
    " \"fibers_dv\" : \"18.0\"," +
    " \"fibers_un\" : \"g\"," +
    " \"sucrose_a\" : \"-\", " +
    " \"sucrose_dv\" : \"-\"," +
    " \"sucrose_un\" : \"-\"," +
    " \"cholesterol_a\" : \"-\"," +
    " \"cholesterol_dv\" : \"-\"," +
    " \"cholesterol_un\" : \"-\"," +
    " \"iron_a\" : \"0.4\"," +
    " \"iron_dv\" : \"3.0\"," +
    " \"iron_un\" : \"mg\"," +
    " \"zync_a\" : \"0.6\"," +
    " \"zync_dv\" : \"9.0\"," +
    " \"zync_un\" : \"mg\"," +
    " \"calcium_a\" : \"112.2\"," +
    " \"calcium_dv\" : \"11.0\"," +
    " \"calcium_un\" : \"mg\"," +
    " \"vitamin_a_a\" : \"-\"," +
    " \"vitamin_a_dv\" : \"-\"," +
    " \"vitamin_a_un\" : \"-\"," +
    " \"vitamin_c_a\" : \"5.6\"," +
    " \"vitamin_c_dv\" : \"12.0\"," +
    " \"vitamin_c_un\" : \"mg\"," +
    " \"vitamin_d_a\" : \"-\"," +
    " \"vitamin_d_dv\" : \"-\"," +
    " \"vitamin_d_un\" : \"-\"," +
    " \"vitamin_e_a\" : \"-\"," +
    " \"vitamin_e_dv\" : \"-\"," +
    " \"vitamin_e_un\" : \"-\"," +
    " \"vitamin_b1_a\" : \"0.1\"," +
    " \"vitamin_b1_dv\" : \"7.0\"," +
    " \"vitamin_b1_un\" : \"mg\"," +
    " \"vitamin_b3_a\" : \"-\"," +
    " \"vitamin_b3_dv\" : \"-\"," +
    " \"vitamin_b3_un\" : \"-\"," +
    " \"vitamin_b6_a\" : \"0\"," +
    " \"vitamin_b6_dv\" : \"0\"," +
    " \"vitamin_b6_un\" : \"mg\"," +
    " \"vitamin_b9_a\" : \"-\"," +
    " \"vitamin_b9_dv\" : \"-\"," +
    " \"vitamin_b9_un\" : \"-\"," +
    " \"vitamin_b12_a\" : \"-\"," +
    " \"vitamin_b12_dv\" : \"-\"," +
    " \"vitamin_b12_un\" : \"-\"},"
        "{" +
    "\"name\" : \"Pêra Park Crua\"," +
    "\"info_about\" : \"A fruta é reconhecida pela sua textura mais encorpada, grandes quantidades de fibras, além das suas conhecidas propriedades nutritivas, que incluem a presença de vitaminas do complexo B, que protegem o sistema cardiovascular, preservam o Sistema Nervoso Central, agem positivamente sobre o sistema digestivo.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"60.6\", " +
    "\"energy_value_dv\" : \"3.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"16.1\"," +
    "\"carbohydrates_dv\" : \"5.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"0.2\"," +
    "\"protein_dv\" : \"0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"0.1\"," +
    "\"sat_fat_dv\" : \"0\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"-\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"-\"," +
    "\"pluns_fat_a\" : \"0.1\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"3.0\"," +
    "\"fibers_dv\" : \"12.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"-\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"-\"," +
    "\"iron_a\" : \"0.3\"," +
    "\"iron_dv\" : \"2.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.1\"," +
    "\"zync_dv\" : \"1.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"8.7\"," +
    "\"calcium_dv\" : \"1.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"-\"," +
    "\"vitamin_c_a\" : \"2.4\"," +
    "\"vitamin_c_dv\" : \"5.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"0.1\"," +
    "\"vitamin_b1_dv\" : \"7.0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"-\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"-\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"}," +
    "{" +
    "\"name\" : \"Rabanete Cru\"," +
    "\"info_about\" : \"As variedades de rabanete possuem uma essência sulfurada com propriedades coleréticas, colagogas, digestivas, antibióticas e mucolíticas.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"13.7\", " +
    "\"energy_value_dv\" : \"1.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"2.7\"," +
    "\"carbohydrates_dv\" : \"1.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"1.4\"," +
    "\"protein_dv\" : \"2.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"-\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"-\"," +
    "\"mnuns_fat_a\" : \"-\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"-\"," +
    "\"pluns_fat_a\" : \"-\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"-\"," +
    "\"fibers_a\" : \"2.20\"," +
    "\"fibers_dv\" : \"9.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"-\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"-\"," +
    "\"iron_a\" : \"0.4\"," +
    "\"iron_dv\" : \"3.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.2\"," +
    "\"zync_dv\" : \"3.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"20.9\"," +
    "\"calcium_dv\" : \"9.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"-\"," +
    "\"vitamin_c_a\" : \"9.6\"," +
    "\"vitamin_c_dv\" : \"21.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"0.1\"," +
    "\"vitamin_b1_dv\" : \"7.0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"0\"," +
    "\"vitamin_b6_dv\" : \"0\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"}," +
    "{" +
    "\"name\" : \"Repolho Roxo Cru\"," +
    "\"info_about\" : \"rico em antocianinas, substâncias capazes de proteger o coração. Além disso, é rico em vitamina C, B6, fibras, potássio e fósforo. Comer o legume cru é a melhor forma de aproveitar estes nutrientes.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"30.9\", " +
    "\"energy_value_dv\" : \"2.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"7.2\"," +
    "\"carbohydrates_dv\" : \"2.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"1.9\"," +
    "\"protein_dv\" : \"3.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"-\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"-\"," +
    "\"mnuns_fat_a\" : \"-\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"-\"," +
    "\"pluns_fat_a\" : \"-\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"-\"," +
    "\"fibers_a\" : \"2.0\"," +
    "\"fibers_dv\" : \"8.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"-\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"-\"," +
    "\"iron_a\" : \"0.5\"," +
    "\"iron_dv\" : \"4.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.3\"," +
    "\"zync_dv\" : \"4.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"43.7\"," +
    "\"calcium_dv\" : \"4.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"-\"," +
    "\"vitamin_c_a\" : \"43.2\"," +
    "\"vitamin_c_dv\" : \"97.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"0.1\"," +
    "\"vitamin_b1_dv\" : \"7.0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"8.0\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"}," +
    "{" +
    "\"name\" : \"Rúcula Crua\"," +
    "\"info_about\" : \"É rica em proteínas, vitaminas A e C e sais minerais, principalmente de cálcio e ferro. Contém também ômega 3, mas é pobre em calorias.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"13.1\", " +
    "\"energy_value_dv\" : \"1.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"2.2\"," +
    "\"carbohydrates_dv\" : \"1.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"1.8\"," +
    "\"protein_dv\" : \"2.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"-\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"-\"," +
    "\"mnuns_fat_a\" : \"-\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"-\"," +
    "\"pluns_fat_a\" : \"-\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"-\"," +
    "\"fibers_a\" : \"1.7\"," +
    "\"fibers_dv\" : \"7.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"-\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"-\"," +
    "\"iron_a\" : \"0.9\"," +
    "\"iron_dv\" : \"6.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.2\"," +
    "\"zync_dv\" : \"3.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"116.6\"," +
    "\"calcium_dv\" : \"12.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"-\"," +
    "\"vitamin_c_a\" : \"46.3\"," +
    "\"vitamin_c_dv\" : \"103.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"0\"," +
    "\"vitamin_b1_dv\" : \"0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"-\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"-\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"}," +
    "{" +
    "\"name\" : \"Manga Palmer Crua\"," +
    "\"info_about\" : \"Devido à riqueza de nutrientes que possui, a manga Palmer oferece benefícios variados para os seres humanos. A manga ajuda na redução de peso, pois a quantidade de fibras solúveis em sua composição faz com que aumente a sensação de saciedade do estômago, evitando que o indivíduo coma fora do horário ou mesmo além da conta.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"72.5\", " +
    "\"energy_value_dv\" : \"4.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"19.4\"," +
    "\"carbohydrates_dv\" : \"6.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"0.4\"," +
    "\"protein_dv\" : \"1.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"-\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"-\"," +
    "\"mnuns_fat_a\" : \"-\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"-\"," +
    "\"pluns_fat_a\" : \"-\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"-\"," +
    "\"fibers_a\" : \"1.6\"," +
    "\"fibers_dv\" : \"6.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"-\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"-\"," +
    "\"iron_a\" : \"0.1\"," +
    "\"iron_dv\" : \"1.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.1\"," +
    "\"zync_dv\" : \"1.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"11.6\"," +
    "\"calcium_dv\" : \"1.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"-\"," +
    "\"vitamin_c_a\" : \"65.5\"," +
    "\"vitamin_c_dv\" : \"146.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"0.1\"," +
    "\"vitamin_b1_dv\" : \"7.0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"-\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"-\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"}," +
    "{" +
    "\"name\" : \"Morango Cru\"," +
    "\"info_about\" : \"Morango (Fragaria) é considerado, na linguagem vulgar, como o fruto vermelho do morangueiro, da família das rosáceas. No entanto, em termos científicos não se pode considerar um fruto já que é constituído pelo receptáculo da flor original (composta), em volta do qual se dispõem os frutos (as sementes são visíveis sob a forma de grainhas).\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"30.2\", " +
    "\"energy_value_dv\" : \"2.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"6.8\"," +
    "\"carbohydrates_dv\" : \"2.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"0.9\"," +
    "\"protein_dv\" : \"1.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"-\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"-\"," +
    "\"mnuns_fat_a\" : \"-\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"-\"," +
    "\"pluns_fat_a\" : \"-\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"-\"," +
    "\"fibers_a\" : \"1.7\"," +
    "\"fibers_dv\" : \"7.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"-\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"-\"," +
    "\"iron_a\" : \"0.3\"," +
    "\"iron_dv\" : \"2.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.2\"," +
    "\"zync_dv\" : \"3.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"10.9\"," +
    "\"calcium_dv\" : \"1.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"-\"," +
    "\"vitamin_c_a\" : \"63.6\"," +
    "\"vitamin_c_dv\" : \"141.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"-\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"0\"," +
    "\"vitamin_b6_dv\" : \"0\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"}," +
    "{" +
    "\"name\" : \"Melancia Crua\"," +
    "\"info_about\" : \"Melancia (Citrullus lanatus) é uma planta da família Cucurbitaceae e do seu fruto. Trata-se de uma trepadeira rastejante originária da África.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"32.6\", " +
    "\"energy_value_dv\" : \"2.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"8.1\"," +
    "\"carbohydrates_dv\" : \"3.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"0.9\"," +
    "\"protein_dv\" : \"1.0\"," +
    "\"protein_un\" : \"1.0\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"-\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"-\"," +
    "\"mnuns_fat_a\" : \"-\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"-\"," +
    "\"pluns_fat_a\" : \"-\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"-\"," +
    "\"fibers_a\" : \"0.1\"," +
    "\"fibers_dv\" : \"0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"-\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"-\"," +
    "\"iron_a\" : \"0.2\"," +
    "\"iron_dv\" : \"1.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.1\"," +
    "\"zync_dv\" : \"1.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"7.7\"," +
    "\"calcium_dv\" : \"1.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"-\"," +
    "\"vitamin_c_a\" : \"6.2\"," +
    "\"vitamin_c_dv\" : \"14.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"-\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"-\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"-\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"}," +
    "{" +
    "\"name\" : \"Melão Cru\"," +
    "\"info_about\" : \"O melão é uma fruta com baixas calorias, bastante rica nutricionalmente e que pode ser usada para emagrecer e hidratar a pele, além de ser rica em vitamina A e flavonoides, poderosos antioxidantes que atuam prevenindo problemas como doenças cardíacas e envelhecimento precoce.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"29.4\", " +
    "\"energy_value_dv\" : \"1.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"7.5\"," +
    "\"carbohydrates_dv\" : \"3.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"0.7\"," +
    "\"protein_dv\" : \"1.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"-\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"-\"," +
    "\"mnuns_fat_a\" : \"-\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"-\"," +
    "\"pluns_fat_a\" : \"-\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"-\"," +
    "\"fibers_a\" : \"0.3\"," +
    "\"fibers_dv\" : \"1.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"-\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"-\"," +
    "\"iron_a\" : \"0.2\"," +
    "\"iron_dv\" : \"1.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.1\"," +
    "\"zync_dv\" : \"1.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"2.9\"," +
    "\"calcium_dv\" : \"0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"-\"," +
    "\"vitamin_c_a\" : \"8.7\"," +
    "\"vitamin_c_dv\" : \"19.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"-\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"0\"," +
    "\"vitamin_b6_dv\" : \"0\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"}," +
    "{" +
    " \"name\" : \"Romã\"," +
    " \"info_about\" : \"A romã é uma infrutescência da romãzeira (Punica granatum), fruto vulgar no mediterrâneo oriental e médio oriente onde é tomado como aperitivo, sobremesa ou algumas vezes em bebida alcoólica. O seu interior é subdividido por finas películas, que formam pequenas sementes possuidoras de uma polpa comestível.\"," +
    " \"standard_portion\" : \"100.0\", " +
    " \"energy_value_a\" : \"55.7\", " +
    " \"energy_value_dv\" : \"3.0\"," +
    " \"energy_value_un\" : \"kcal\"," +
    " \"carbohydrates_a\" : \"15.1\"," +
    " \"carbohydrates_dv\" : \"5.0\"," +
    " \"carbohydrates_un\" : \"g\"," +
    " \"sugar_a\" : \"-\"," +
    " \"sugar_dv\" : \"-\"," +
    " \"sugar_un\" : \"-\"," +
    " \"protein_a\" : \"0.4\"," +
    " \"protein_dv\" : \"1.0\"," +
    " \"protein_un\" : \"g\"," +
    " \"trans_fat_a\" : \"-\"," +
    " \"trans_fat_dv\" : \"-\"," +
    " \"trans_fat_un\" : \"-\"," +
    " \"sat_fat_a\" : \"-\"," +
    " \"sat_fat_dv\" : \"-\"," +
    " \"sat_fat_un\" : \"-\"," +
    " \"mnuns_fat_a\" : \"-\"," +
    " \"mnuns_fat_dv\" : \"-\"," +
    " \"mnuns_fat_un\" : \"-\"," +
    " \"pluns_fat_a\" : \"-\", " +
    " \"pluns_fat_dv\" : \"-\"," +
    " \"pluns_fat_un\" : \"-\"," +
    " \"fibers_a\" : \"0.4\"," +
    " \"fibers_dv\" : \"2.0\"," +
    " \"fibers_un\" : \"2.0\"," +
    " \"sucrose_a\" : \"-\", " +
    " \"sucrose_dv\" : \"-\"," +
    " \"sucrose_un\" : \"-\"," +
    " \"cholesterol_a\" : \"-\"," +
    " \"cholesterol_dv\" : \"-\"," +
    " \"cholesterol_un\" : \"-\"," +
    " \"iron_a\" : \"0.3\"," +
    " \"iron_dv\" : \"2.0\"," +
    " \"iron_un\" : \"mg\"," +
    " \"zync_a\" : \"0.7\"," +
    " \"zync_dv\" : \"10.0\"," +
    " \"zync_un\" : \"mg\"," +
    " \"calcium_a\" : \"4.8\"," +
    " \"calcium_dv\" : \"4.8\"," +
    " \"calcium_un\" : \"mg\"," +
    " \"vitamin_a_a\" : \"-\"," +
    " \"vitamin_a_dv\" : \"-\"," +
    " \"vitamin_a_un\" : \"-\"," +
    " \"vitamin_c_a\" : \"8.1\"," +
    " \"vitamin_c_dv\" : \"18.0\"," +
    " \"vitamin_c_un\" : \"mg\"," +
    " \"vitamin_d_a\" : \"-\"," +
    " \"vitamin_d_dv\" : \"-\"," +
    " \"vitamin_d_un\" : \"-\"," +
    " \"vitamin_e_a\" : \"-\"," +
    " \"vitamin_e_dv\" : \"-\"," +
    " \"vitamin_e_un\" : \"-\"," +
    " \"vitamin_b1_a\" : \"0.1\"," +
    " \"vitamin_b1_dv\" : \"7.0\"," +
    " \"vitamin_b1_un\" : \"mg\"," +
    " \"vitamin_b3_a\" : \"-\"," +
    " \"vitamin_b3_dv\" : \"-\"," +
    " \"vitamin_b3_un\" : \"-\"," +
    " \"vitamin_b6_a\" : \"0.1\"," +
    " \"vitamin_b6_dv\" : \"0.1\"," +
    " \"vitamin_b6_un\" : \"8.0\"," +
    " \"vitamin_b9_a\" : \"mg\"," +
    " \"vitamin_b9_dv\" : \"-\"," +
    " \"vitamin_b9_un\" : \"-\"," +
    " \"vitamin_b12_a\" : \"-\"," +
    " \"vitamin_b12_dv\" : \"-\"," +
    " \"vitamin_b12_un\" : \"-\"}," +
    "{" +
    " \"name\" : \"Mamão Papaia Cru\"," +
    " \"info_about\" : \"O consumo do mamão é recomendado pelos nutricionistas por se constituir em um alimento rico em licopeno, vitamina C e minerais importantes para o organismo. Quanto mais maduro, maior a concentração desses nutrientes.\"," +
    " \"standard_portion\" : \"100.0\", " +
    " \"energy_value_a\" : \"40.2\", " +
    " \"energy_value_dv\" : \"2.0\"," +
    " \"energy_value_un\" : \"kcal\"," +
    " \"carbohydrates_a\" : \"10.4\"," +
    " \"carbohydrates_dv\" : \"3.0\"," +
    " \"carbohydrates_un\" : \"g\"," +
    " \"sugar_a\" : \"-\"," +
    " \"sugar_dv\" : \"-\"," +
    " \"sugar_un\" : \"-\"," +
    " \"protein_a\" : \"0.5\"," +
    " \"protein_dv\" : \"1.0\"," +
    " \"protein_un\" : \"g\"," +
    " \"trans_fat_a\" : \"-\"," +
    " \"trans_fat_dv\" : \"-\"," +
    " \"trans_fat_un\" : \"-\"," +
    " \"sat_fat_a\" : \"-\"," +
    " \"sat_fat_dv\" : \"-\"," +
    " \"sat_fat_un\" : \"-\"," +
    " \"mnuns_fat_a\" : \"-\"," +
    " \"mnuns_fat_dv\" : \"-\"," +
    " \"mnuns_fat_un\" : \"-\"," +
    " \"pluns_fat_a\" : \"-\", " +
    " \"pluns_fat_dv\" : \"-\"," +
    " \"pluns_fat_un\" : \"-\"," +
    " \"fibers_a\" : \"-\"," +
    " \"fibers_dv\" : \"-\"," +
    " \"fibers_un\" : \"-\"," +
    " \"sucrose_a\" : \"-\", " +
    " \"sucrose_dv\" : \"-\"," +
    " \"sucrose_un\" : \"-\"," +
    " \"cholesterol_a\" : \"-\"," +
    " \"cholesterol_dv\" : \"-\"," +
    " \"cholesterol_un\" : \"-\"," +
    " \"iron_a\" : \"0.2\"," +
    " \"iron_dv\" : \"1.0\"," +
    " \"iron_un\" : \"mg\"," +
    " \"zync_a\" : \"0.1\"," +
    " \"zync_dv\" : \"1.0\"," +
    " \"zync_un\" : \"mg\"," +
    " \"calcium_a\" : \"22.4\"," +
    " \"calcium_dv\" : \"2.0\"," +
    " \"calcium_un\" : \"mg\"," +
    " \"vitamin_a_a\" : \"-\"," +
    " \"vitamin_a_dv\" : \"-\"," +
    " \"vitamin_a_un\" : \"-\"," +
    " \"vitamin_c_a\" : \"82.2\"," +
    " \"vitamin_c_dv\" : \"183.0\"," +
    " \"vitamin_c_un\" : \"mg\"," +
    " \"vitamin_d_a\" : \"-\"," +
    " \"vitamin_d_dv\" : \"-\"," +
    " \"vitamin_d_un\" : \"-\"," +
    " \"vitamin_e_a\" : \"-\"," +
    " \"vitamin_e_dv\" : \"-\"," +
    " \"vitamin_e_un\" : \"-\"," +
    " \"vitamin_b1_a\" : \"0\"," +
    " \"vitamin_b1_dv\" : \"0\"," +
    " \"vitamin_b1_un\" : \"mg\"," +
    " \"vitamin_b3_a\" : \"-\"," +
    " \"vitamin_b3_dv\" : \"-\"," +
    " \"vitamin_b3_un\" : \"-\"," +
    " \"vitamin_b6_a\" : \"-\"," +
    " \"vitamin_b6_dv\" : \"-\"," +
    " \"vitamin_b6_un\" : \"-\"," +
    " \"vitamin_b9_a\" : \"-\"," +
    " \"vitamin_b9_dv\" : \"-\"," +
    " \"vitamin_b9_un\" : \"-\"," +
    " \"vitamin_b12_a\" : \"-\"," +
    " \"vitamin_b12_dv\" : \"-\"," +
    " \"vitamin_b12_un\" : \"-\"}," +
    "{" +
    " \"name\" : \"Noz Crua\"," +
    " \"info_about\" : \"As nozes, apesar do sabor agradável, por muito tempo foram consideradas inconvenientes para alimentação humana por serem ricas em gordura. Entretanto algumas dessas oleaginosas, trazem um grande benefício para a saúde pois ajudam a controlar o colesterol ruim e por consequência ajudam a proteger o coração.\"," +
    " \"standard_portion\" : \"100.0\", " +
    " \"energy_value_a\" : \"620.1\", " +
    " \"energy_value_dv\" : \"31.0\"," +
    " \"energy_value_un\" : \"kcal\"," +
    " \"carbohydrates_a\" : \"18.4\"," +
    " \"carbohydrates_dv\" : \"6.0\"," +
    " \"carbohydrates_un\" : \"g\"," +
    " \"sugar_a\" : \"-\"," +
    " \"sugar_dv\" : \"-\"," +
    " \"sugar_un\" : \"-\"," +
    " \"protein_a\" : \"14.0\"," +
    " \"protein_dv\" : \"19.0\"," +
    " \"protein_un\" : \"g\"," +
    " \"trans_fat_a\" : \"-\"," +
    " \"trans_fat_dv\" : \"-\"," +
    " \"trans_fat_un\" : \"-\"," +
    " \"sat_fat_a\" : \"5.6\"," +
    " \"sat_fat_dv\" : \"25.0\"," +
    " \"sat_fat_un\" : \"g\"," +
    " \"mnuns_fat_a\" : \"8.7\"," +
    " \"mnuns_fat_dv\" : \"-\"," +
    " \"mnuns_fat_un\" : \"g\"," +
    " \"pluns_fat_a\" : \"44.1\", " +
    " \"pluns_fat_dv\" : \"-\"," +
    " \"pluns_fat_un\" : \"g\"," +
    " \"fibers_a\" : \"7.3\"," +
    " \"fibers_dv\" : \"29.0\"," +
    " \"fibers_un\" : \"g\"," +
    " \"sucrose_a\" : \"-\", " +
    " \"sucrose_dv\" : \"-\"," +
    " \"sucrose_un\" : \"-\"," +
    " \"cholesterol_a\" : \"-\"," +
    " \"cholesterol_dv\" : \"-\"," +
    " \"cholesterol_un\" : \"-\"," +
    " \"iron_a\" : \"2.0\"," +
    " \"iron_dv\" : \"14.0\"," +
    " \"iron_un\" : \"mg\"," +
    " \"zync_a\" : \"2.1\"," +
    " \"zync_dv\" : \"30.0\"," +
    " \"zync_un\" : \"mg\"," +
    " \"calcium_a\" : \"105.3\"," +
    " \"calcium_dv\" : \"11.0\"," +
    " \"calcium_un\" : \"mg\"," +
    " \"vitamin_a_a\" : \"-\"," +
    " \"vitamin_a_dv\" : \"-\"," +
    " \"vitamin_a_un\" : \"-\"," +
    " \"vitamin_c_a\" : \"-\"," +
    " \"vitamin_c_dv\" : \"-\"," +
    " \"vitamin_c_un\" : \"-\"," +
    " \"vitamin_d_a\" : \"-\"," +
    " \"vitamin_d_dv\" : \"-\"," +
    " \"vitamin_d_un\" : \"-\"," +
    " \"vitamin_e_a\" : \"-\"," +
    " \"vitamin_e_dv\" : \"-\"," +
    " \"vitamin_e_un\" : \"-\"," +
    " \"vitamin_b1_a\" : \"0.4\"," +
    " \"vitamin_b1_dv\" : \"29.0\"," +
    " \"vitamin_b1_un\" : \"mg\"," +
    " \"vitamin_b3_a\" : \"-\"," +
    " \"vitamin_b3_dv\" : \"-\"," +
    " \"vitamin_b3_un\" : \"-\"," +
    " \"vitamin_b6_a\" : \"0.1\"," +
    " \"vitamin_b6_dv\" : \"8.0\"," +
    " \"vitamin_b6_un\" : \"mg\"," +
    " \"vitamin_b9_a\" : \"-\"," +
    " \"vitamin_b9_dv\" : \"-\"," +
    " \"vitamin_b9_un\" : \"-\"," +
    " \"vitamin_b12_a\" : \"-\"," +
    " \"vitamin_b12_dv\" : \"-\"," +
    " \"vitamin_b12_un\" : \"-\"}," +
    "{" +
    " \"name\" : \"Ovo de Galinha Inteiro Frito\"," +
    " \"info_about\" : \"Pode ser consumido, mas é preciso usar uma frigideira antiaderente sem gordura pois os óleos vegetais em alta temperatura se transformam em gordura trans, que é muito prejudicial à saúde e faz com que o ovo perca seus benefícios, pode ser frito também na água, basta colocar uma colher de chá na frigideira e quando ferver colocar o ovo e fritar.\"," +
    " \"standard_portion\" : \"100.0\", " +
    " \"energy_value_a\" : \"240.2\", " +
    " \"energy_value_dv\" : \"12.0\"," +
    " \"energy_value_un\" : \"kcal\"," +
    " \"carbohydrates_a\" : \"1.2\"," +
    " \"carbohydrates_dv\" : \"0\"," +
    " \"carbohydrates_un\" : \"g\"," +
    " \"sugar_a\" : \"-\"," +
    " \"sugar_dv\" : \"-\"," +
    " \"sugar_un\" : \"-\"," +
    " \"protein_a\" : \"15.6\"," +
    " \"protein_dv\" : \"21.0\"," +
    " \"protein_un\" : \"g\"," +
    " \"trans_fat_a\" : \"-\"," +
    " \"trans_fat_dv\" : \"-\"," +
    " \"trans_fat_un\" : \"-\"," +
    " \"sat_fat_a\" : \"4.1\"," +
    " \"sat_fat_dv\" : \"19.0\"," +
    " \"sat_fat_un\" : \"g\"," +
    " \"mnuns_fat_a\" : \"5.7\"," +
    " \"mnuns_fat_dv\" : \"-\"," +
    " \"mnuns_fat_un\" : \"g\"," +
    " \"pluns_fat_a\" : \"4.9\", " +
    " \"pluns_fat_dv\" : \"-\"," +
    " \"pluns_fat_un\" : \"g\"," +
    " \"fibers_a\" : \"-\"," +
    " \"fibers_dv\" : \"-\"," +
    " \"fibers_un\" : \"-\"," +
    " \"sucrose_a\" : \"-\", " +
    " \"sucrose_dv\" : \"-\"," +
    " \"sucrose_un\" : \"-\"," +
    " \"cholesterol_a\" : \"516.3\"," +
    " \"cholesterol_dv\" : \"-\"," +
    " \"cholesterol_un\" : \"mg\"," +
    " \"iron_a\" : \"2.1\"," +
    " \"iron_dv\" : \"15.0\"," +
    " \"iron_un\" : \"mg\"," +
    " \"zync_a\" : \"1.5\"," +
    " \"zync_dv\" : \"21.0\"," +
    " \"zync_un\" : \"mg\"," +
    " \"calcium_a\" : \"72.9\"," +
    " \"calcium_dv\" : \"7.0\"," +
    " \"calcium_un\" : \"mg\"," +
    " \"vitamin_a_a\" : \"93.9\"," +
    " \"vitamin_a_dv\" : \"16.0\"," +
    " \"vitamin_a_un\" : \"ug\"," +
    " \"vitamin_c_a\" : \"-\"," +
    " \"vitamin_c_dv\" : \"-\"," +
    " \"vitamin_c_un\" : \"-\"," +
    " \"vitamin_d_a\" : \"-\"," +
    " \"vitamin_d_dv\" : \"-\"," +
    " \"vitamin_d_un\" : \"-\"," +
    " \"vitamin_e_a\" : \"-\"," +
    " \"vitamin_e_dv\" : \"-\"," +
    " \"vitamin_e_un\" : \"-\"," +
    " \"vitamin_b1_a\" : \"-\"," +
    " \"vitamin_b1_dv\" : \"-\"," +
    " \"vitamin_b1_un\" : \"-\"," +
    " \"vitamin_b3_a\" : \"-\"," +
    " \"vitamin_b3_dv\" : \"-\"," +
    " \"vitamin_b3_un\" : \"-\"," +
    " \"vitamin_b6_a\" : \"-\"," +
    " \"vitamin_b6_dv\" : \"-\"," +
    " \"vitamin_b6_un\" : \"-\"," +
    " \"vitamin_b9_a\" : \"-\"," +
    " \"vitamin_b9_dv\" : \"-\"," +
    " \"vitamin_b9_un\" : \"-\"," +
    " \"vitamin_b12_a\" : \"-\"," +
    " \"vitamin_b12_dv\" : \"-\"," +
    " \"vitamin_b12_un\" : \"-\"},{" +
    "\"name\" : \"Pitanga Crua\"," +
    "\"info_about\" : \"A pitanga é o fruto da pitangueira (Eugenia uniflora L.), dicotiledônea da família das mirtáceas. Tem a forma de bolinhas globosas e carnosas, de cor vermelha (a mais comum), laranja, amarela ou preta. Na mesma árvore, o fruto poderá ter desde as cores verde, amarelo e alaranjado até a cor vermelho-intenso, de acordo com o grau de maturação.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"41.4\", " +
    "\"energy_value_dv\" : \"2.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"10.2\"," +
    "\"carbohydrates_dv\" : \"3.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"0.9\"," +
    "\"protein_dv\" : \"1.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"-\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"-\"," +
    "\"mnuns_fat_a\" : \"-\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"-\"," +
    "\"pluns_fat_a\" : \"-\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"-\"," +
    "\"fibers_a\" : \"3.20\"," +
    "\"fibers_dv\" : \"13.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"-\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"-\"," +
    "\"iron_a\" : \"0.4\"," +
    "\"iron_dv\" : \"3.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.4\"," +
    "\"zync_dv\" : \"6.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"17.9\"," +
    "\"calcium_dv\" : \"2.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"-\"," +
    "\"vitamin_c_a\" : \"24.9\"," +
    "\"vitamin_c_dv\" : \"55.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"0\"," +
    "\"vitamin_b1_dv\" : \"0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"-\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"-\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"},{" +
    "\"name\" : \"Pupunha Cozida\"," +
    "\"info_about\" : \"Bactris gasipaes (Kunth) é uma espécie de palmeira multicaule da família Arecaceae cujo fruto é conhecido por pupunha ou babunha. A espécie é nativa da região amazônica onde é conhecida popularmente pelos nomes pupunheira e pupunha-verde-amarela. Os frutos alaranjados são ricos em proteínas, amidos e vitamina A, frequentemente consumidos depois de cozidos em água e sal, ou na forma de farinha ou óleo comestíveis.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"218.5\", " +
    "\"energy_value_dv\" : \"11.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"29.6\"," +
    "\"carbohydrates_dv\" : \"10.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"2.5\"," +
    "\"protein_dv\" : \"3.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"3.1\"," +
    "\"sat_fat_dv\" : \"14.0\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"6.8\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0.4\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"4.3\"," +
    "\"fibers_dv\" : \"17.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"-\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"-\"," +
    "\"iron_a\" : \"0.5\"," +
    "\"iron_dv\" : \"4.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.3\"," +
    "\"zync_dv\" : \"4.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"27.6\"," +
    "\"calcium_dv\" : \"3.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"-\"," +
    "\"vitamin_c_a\" : \"2.2\"," +
    "\"vitamin_c_dv\" : \"5.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"-\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"0\"," +
    "\"vitamin_b6_dv\" : \"0\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"}]";

String jsonData2 = "[{ " +
    "\"name\" : \"Tangerina Poncã\","
        "\"info_about\" : \"A tangerina, também conhecida como tangerina ou tangerina, é uma pequena árvore cítrica com frutas semelhantes a outras laranjas, geralmente consumidas simples ou em saladas de frutas.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"3.8\", " +
    "\"energy_value_dv\" : \"1.9\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"9.6\"," +
    "\"carbohydrates_dv\" : \"3.6\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"0.8\"," +
    "\"protein_dv\" : \"1.6\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0\"," +
    "\"sat_fat_dv\" : \"0\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"0.9\"," +
    "\"fibers_dv\" : \"3.6\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.1\"," +
    "\"iron_dv\" : \"1.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"-\"," +
    "\"zync_dv\" : \"-\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"13.0\"," +
    "\"calcium_dv\" : \"1.6\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"48.6\"," +
    "\"vitamin_c_dv\" : \"-\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"-\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Tabule\"," +
    "\"info_about\" : \"Tabule é um prato libanês de salada, frequentemente degustado como um aperitivo. É feito principalmente de triguilho, tomate, cebola, salsa, hortelã e outras ervas, com suco de limão, pimenta e vários temperos. No Líbano, onde surgiu, é consumido por cima de folhas de alface.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"57.0\", " +
    "\"energy_value_dv\" : \"2.8\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"10.6\"," +
    "\"carbohydrates_dv\" : \"3.5\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"2.2\"," +
    "\"protein_dv\" : \"4.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0.1\"," +
    "\"sat_fat_dv\" : \"1.0\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0.7\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"2.1\"," +
    "\"fibers_dv\" : \"8.4\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.6\"," +
    "\"iron_dv\" : \"6.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.6\"," +
    "\"zync_dv\" : \"4.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"19.0\"," +
    "\"calcium_dv\" : \"2.3\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"-\"," +
    "\"vitamin_c_dv\" : \"-\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Tomate cru\"," +
    "\"info_about\" : \"Um dos frutos mais usados na culinária em todo o mundo, o tomate é rico em vitaminas, em sais minerais como o fósforo, potássio, cálcio e magnésio. Esses nutrientes ajudam no desenvolvimento de dentes, músculos e ossos. Ainda auxiliam na proteção do sistema imunológico, entre muitos outros benefícios.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"15.0\", " +
    "\"energy_value_dv\" : \"0.7\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"3.1\"," +
    "\"carbohydrates_dv\" : \"1.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"1.1\"," +
    "\"protein_dv\" : \"2.2\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0\"," +
    "\"sat_fat_dv\" : \"0\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"1.2\"," +
    "\"fibers_dv\" : \"4.8\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.2\"," +
    "\"iron_dv\" : \"2.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.1\"," +
    "\"zync_dv\" : \"0.6\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"7.0\"," +
    "\"calcium_dv\" : \"0.8\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"1131.0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"-\"," +
    "\"vitamin_c_dv\" : \"-\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"0.4\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Yakisoba\"," +
    "\"info_about\" : \"Sōsu yakissoba, também conhecido por yakisoba, embora muitos pensem que tenha sido criado na China na verdade é um prato de origem japonesa, cujo nome significa, literalmente, 'macarrão de sobá frito'.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"113.0\", " +
    "\"energy_value_dv\" : \"5.6\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"18.3\"," +
    "\"carbohydrates_dv\" : \"6.1\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"7.5\"," +
    "\"protein_dv\" : \"15.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0.2\"," +
    "\"sat_fat_dv\" : \"3.0\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0.1\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0.9\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"1.2\"," +
    "\"fibers_dv\" : \"4.8\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.2\"," +
    "\"iron_dv\" : \"2.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.1\"," +
    "\"zync_dv\" : \"0.6\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"7.0\"," +
    "\"calcium_dv\" : \"0.8\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"-\"," +
    "\"vitamin_c_dv\" : \"-\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"0.4\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Vagem crua\"," +
    "\"info_about\" : \"Vagem é o fruto verde de vários cultivos de feijão comum.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"25.0\", " +
    "\"energy_value_dv\" : \"1.2\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"5.3\"," +
    "\"carbohydrates_dv\" : \"1.7\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"1.8\"," +
    "\"protein_dv\" : \"3.6\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0.1\"," +
    "\"sat_fat_dv\" : \"0.9\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0.1\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"2.4\"," +
    "\"fibers_dv\" : \"9.6\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.4\"," +
    "\"iron_dv\" : \"4.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.3\"," +
    "\"zync_dv\" : \"2.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"41.0\"," +
    "\"calcium_dv\" : \"5.1\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"690.0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"12.2\"," +
    "\"vitamin_c_dv\" : \"-\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Tamarindo\","
        "\"info_about\" : \"É laxativo e diurético. O tamarindo ajuda a regular não apenas o aparelho digestivo como também o sistema urinário.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"276.0\", " +
    "\"energy_value_dv\" : \"13.8\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"72.5\"," +
    "\"carbohydrates_dv\" : \"24.1\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"3.2\"," +
    "\"protein_dv\" : \"6.4\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0.1\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0.2\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0.1\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"6.4\"," +
    "\"fibers_dv\" : \"25.6\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.6\"," +
    "\"iron_dv\" : \"6.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.7\"," +
    "\"zync_dv\" : \"4.6\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"37.0\"," +
    "\"calcium_dv\" : \"4.6\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"7.3\"," +
    "\"vitamin_c_dv\" : \"16.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Acelga\"," +
    "\"info_about\" : \"A acelga ou beterraba branca (Beta vulgaris var. cicla) é uma hortaliça que apresenta talos longos e firmes e folhas baças ou brilhantes, com coloração verde ou avermelhada.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"21.0\", " +
    "\"energy_value_dv\" : \"1.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"4.5\"," +
    "\"carbohydrates_dv\" : \"4.4\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"1.4\"," +
    "\"protein_dv\" : \"2.8\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0.1\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"1.1\"," +
    "\"fibers_dv\" : \"4.4\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.3\"," +
    "\"iron_dv\" : \"3.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.3\"," +
    "\"zync_dv\" : \"2.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"43.0\"," +
    "\"calcium_dv\" : \"5.3\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"6116.0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"30.0\"," +
    "\"vitamin_c_dv\" : \"0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Alface\","
        "\"info_about\" : \"Alface é uma hortense anual ou bienal, utilizada na alimentação humana desde cerca de 500 a.C.. Originária do Leste do Mediterrâneo, é mundialmente cultivada para o consumo em saladas, com inúmeras variedades de folhas, cores, formas, tamanhos e texturas.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"9.0\", " +
    "\"energy_value_dv\" : \"0.4\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"1.7\"," +
    "\"carbohydrates_dv\" : \"0.5\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"0.6\"," +
    "\"protein_dv\" : \"1.2\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0.1\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"1.0\"," +
    "\"fibers_dv\" : \"4.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.3\"," +
    "\"iron_dv\" : \"3.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.2\"," +
    "\"zync_dv\" : \"1.3\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"14.0\"," +
    "\"calcium_dv\" : \"1.7\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"7405.0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"9.2\"," +
    "\"vitamin_c_dv\" : \"0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Almeirão\"," +
    "\"info_about\" : \"O almeirão (Cichorium intybus intybus) é uma variedade de chicória-comum, da família das Asteraceae. Muito semelhante às outras chicórias-comuns, delas se diferencia por possuir folhas mais alongadas, mais estreitas, recobertas por pelos e com sabor amargo mais pronunciado.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"18.0\", " +
    "\"energy_value_dv\" : \"0.9\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"3.3\"," +
    "\"carbohydrates_dv\" : \"1.1\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"1.8\"," +
    "\"protein_dv\" : \"3.6\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0.1\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"1.0\"," +
    "\"fibers_dv\" : \"4.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.3\"," +
    "\"iron_dv\" : \"3.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.2\"," +
    "\"zync_dv\" : \"1.3\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"14.0\"," +
    "\"calcium_dv\" : \"1.7\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"1.7\"," +
    "\"vitamin_c_dv\" : \"4.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"0,1\"," +
    "\"vitamin_b1_dv\" : \"7.0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Banana prata\"," +
    "\"info_about\" : \"Este tipo da fruta possui um formato reto, polpa bem amarelada e casca com cinco facetas (divisões). Se você puder provar antes de comprar, experimente um pequeno pedaço. Geralmente, a banana prata é menos doce que a nanica e sua polpa, além de mais amarela, é mais consistente que as demais variedades.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"98.0\", " +
    "\"energy_value_dv\" : \"4.9\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"26.0\"," +
    "\"carbohydrates_dv\" : \"8.6\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"1.3\"," +
    "\"protein_dv\" : \"2.6\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0.1\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0.2\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0.1\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"2.0\"," +
    "\"fibers_dv\" : \"8.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.4\"," +
    "\"iron_dv\" : \"4.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.1\"," +
    "\"zync_dv\" : \"0.6\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"8.0\"," +
    "\"calcium_dv\" : \"1.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"68.0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"1.7\"," +
    "\"vitamin_c_dv\" : \"4.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"0.1\"," +
    "\"vitamin_b1_dv\" : \"7.0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Maçã\"," +
    "\"info_about\" : \"A concentração de fibras e vitaminas B, C e E faz desta fruta uma importante aliada na prevenção de doenças. Caso não saiba do poder da maçã, conheça dez benefícios que esta fruta pode trazer. Diabates: A maçã é rica em pectina, uma fibra que ajuda no controle da glicemia.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"55.5\", " +
    "\"energy_value_dv\" : \"0.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"15.2\"," +
    "\"carbohydrates_dv\" : \"5.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"10.0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"0.3\"," +
    "\"protein_dv\" : \"0.1\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0.1\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0.1\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"1.4\"," +
    "\"fibers_dv\" : \"6.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.1\"," +
    "\"iron_dv\" : \"1.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"-\"," +
    "\"zync_dv\" : \"0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"1.9\"," +
    "\"calcium_dv\" : \"0.1\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"54.0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"4.6\"," +
    "\"vitamin_c_dv\" : \"4.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"0\"," +
    "\"vitamin_b1_dv\" : \"0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Manteiga com sal\"," +
    "\"info_about\" : \"A manteiga contém uma variedade imensa de vitaminas e minerais, ácido láurico, que ajuda a prevenir o organismo contra infecções fúngicas. Rica em antioxidantes que ajudam a proteger o corpo contra os danos celulares.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"726.0\", " +
    "\"energy_value_dv\" : \"36.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"0.1\"," +
    "\"carbohydrates_dv\" : \"0.2\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0.1\"," +
    "\"sugar_dv\" : \"0.2\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"0.3\"," +
    "\"protein_dv\" : \"0.1\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"49.2.0\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"21.0\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"1.2\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"1.4\"," +
    "\"fibers_dv\" : \"6.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.1\"," +
    "\"iron_dv\" : \"1.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"-\"," +
    "\"zync_dv\" : \"0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"1.9\"," +
    "\"calcium_dv\" : \"0.1\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"2499.0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"4.6\"," +
    "\"vitamin_c_dv\" : \"4.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"60.0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"0\"," +
    "\"vitamin_b1_dv\" : \"0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Margarina com sal\"," +
    "\"info_about\" : \"A margarina, como é actualmente concebida, e os cremes vegetais para barrar têm benefícios claros para a saúde, graças às suas gorduras essenciais, como os ómegas 3 e 6, e as vitaminas A, D e E.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"594.5\", " +
    "\"energy_value_dv\" : \"30.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"0\"," +
    "\"carbohydrates_dv\" : \"0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0.1\"," +
    "\"sugar_dv\" : \"0.2\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"0.2\"," +
    "\"protein_dv\" : \"0.1\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"15.0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"15.0\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"39.0\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"24.0\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"0\"," +
    "\"fibers_dv\" : \"0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.1\"," +
    "\"iron_dv\" : \"1.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"-\"," +
    "\"zync_dv\" : \"0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"6.0\"," +
    "\"calcium_dv\" : \"0.7\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"3577.0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"0.2\"," +
    "\"vitamin_c_dv\" : \".0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"60.0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"0\"," +
    "\"vitamin_b1_dv\" : \"0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Catchup\"," +
    "\"info_about\" : \"O tomate é riquíssimo em licopeno, que é um importante protetor celular graças a sua ação antioxidante. Isso significa que ele pode melhorar a beleza da pele e evitar doenças cardiovasculares, problemas visuais e até mesmo o câncerA margarina, como é actualmente concebida, e os cremes vegetais para barrar têm benefícios claros para a saúde, graças às suas gorduras essenciais, como os ómegas 3 e 6, e as vitaminas A, D e E.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"594.5\", " +
    "\"energy_value_dv\" : \"30.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"0\"," +
    "\"carbohydrates_dv\" : \"0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0.1\"," +
    "\"sugar_dv\" : \"22.2\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"1.3\"," +
    "\"protein_dv\" : \"0.1\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0.0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0.1\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0.1\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0.1\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"0.3\"," +
    "\"fibers_dv\" : \"0.4\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.4\"," +
    "\"iron_dv\" : \"4.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"-\"," +
    "\"zync_dv\" : \"0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"16.0\"," +
    "\"calcium_dv\" : \"2.3\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"513.0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"4.1\"," +
    "\"vitamin_c_dv\" : \".0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"60.0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"0\"," +
    "\"vitamin_b1_dv\" : \"0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.2\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"},{" +
    "\"name\" : \"Salsa Crua\"," +
    "\"info_about\" : \"A salsa fresca é rica em vitaminas e a sua celulose ajuda o movimento intestinal. Além de seu largo uso decorativo, a salsinha provê vários benefícios à saúde. É uma boa fonte de antioxidantes (especialmente luteolina), ácido fólico, vitamina C e vitamina A. Entre os benefícios à saúde declarados estão propriedades anti-inflamatórias e melhora no sistema imune.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"33.4\", " +
    "\"energy_value_dv\" : \"2.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"5.7\"," +
    "\"carbohydrates_dv\" : \"2.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"3.3\"," +
    "\"protein_dv\" : \"4.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"0.1\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"-\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"-\"," +
    "\"pluns_fat_a\" : \"0.2\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"1.9\"," +
    "\"fibers_dv\" : \"8.0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"-\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"-\"," +
    "\"iron_a\" : \"3.2\"," +
    "\"iron_dv\" : \"23.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"-\"," +
    "\"zync_dv\" : \"-\"," +
    "\"zync_un\" : \"-\"," +
    "\"calcium_a\" : \"179.4\"," +
    "\"calcium_dv\" : \"18.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"-\"," +
    "\"vitamin_c_a\" : \"51.7\"," +
    "\"vitamin_c_dv\" : \"115.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"0.1\"," +
    "\"vitamin_b1_dv\" : \"7.0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"0.5\"," +
    "\"vitamin_b6_dv\" : \"38.0\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"}," +
    "{" +
    "\"name\" : \"Ovo de Galinha Inteiro Cozido por 10 Minutos\"," +
    "\"info_about\" : \"Cozinhar os ovos é a segunda alternativa recomendada, pois desse modo a gema preserva a maioria de seus nutrientes.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"145.7\", " +
    "\"energy_value_dv\" : \"7.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"0.6\"," +
    "\"carbohydrates_dv\" : \"0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"13.3\"," +
    "\"protein_dv\" : \"18.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"2.9\"," +
    "\"sat_fat_dv\" : \"13.0\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"3.8\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"1.1\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"-\"," +
    "\"fibers_dv\" : \"-\"," +
    "\"fibers_un\" : \"-\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"396.6\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"1.5\"," +
    "\"iron_dv\" : \"11.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"1.2\"," +
    "\"zync_dv\" : \"17.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"49.2\"," +
    "\"calcium_dv\" : \"5.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"32.4\"," +
    "\"vitamin_a_dv\" : \"5.0\"," +
    "\"vitamin_a_un\" : \"ug\"," +
    "\"vitamin_c_a\" : \"-\"," +
    "\"vitamin_c_dv\" : \"-\"," +
    "\"vitamin_c_un\" : \"-\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"0.1\"," +
    "\"vitamin_b1_dv\" : \"7.0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"-\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"-\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"}," +
    "{" +
    "\"name\" : \"Ovo de Galinha Gema Cozida por 10 Minutos\"," +
    "\"info_about\" : \"A gema pode equilibrar o perfil aminoácido da proteína, assim ela fica mais disponível ao corpo. Embora a gema de ovo seja rica em colesterol e gordura, ela não aumenta os riscos de hipertensão e doenças cardíacas se ela for consumida em moderação, de preferência uma por dia.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"352.7\", " +
    "\"energy_value_dv\" : \"18.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"1.6\"," +
    "\"carbohydrates_dv\" : \"1.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"15.9\"," +
    "\"protein_dv\" : \"21.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"9.2\"," +
    "\"sat_fat_dv\" : \"42.0\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"12.1\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"4.0\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"-\"," +
    "\"fibers_dv\" : \"-\"," +
    "\"fibers_un\" : \"-\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"-\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"-\"," +
    "\"iron_a\" : \"2.9\"," +
    "\"iron_dv\" : \"21.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"2.9\"," +
    "\"zync_dv\" : \"41.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"114.4\"," +
    "\"calcium_dv\" : \"11.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"148.5\"," +
    "\"vitamin_a_dv\" : \"25.0\"," +
    "\"vitamin_a_un\" : \"ug\"," +
    "\"vitamin_c_a\" : \"-\"," +
    "\"vitamin_c_dv\" : \"-\"," +
    "\"vitamin_c_un\" : \"-\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"0.2\"," +
    "\"vitamin_b1_dv\" : \"14.0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"-\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"-\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"}," +
    "{" +
    "\"name\" : \"Ovo de Galinha Clara Cozida por 10 Minutos\"," +
    "\"info_about\" : \"Muitas pessoas têm medo de comer ovo por conta do colesterol. No entanto, o colesterol do ovo está presente apenas na gema e só é nocivo a pessoas que já tenham uma predisposição para ter o colesterol elevado por conta dos alimentos. A clara, por outro lado, é segura para todos (a menos para quem tem alergia a algum componente da clara).\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"59.4\", " +
    "\"energy_value_dv\" : \"3.0\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"-\"," +
    "\"carbohydrates_dv\" : \"-\"," +
    "\"carbohydrates_un\" : \"-\"," +
    "\"sugar_a\" : \"-\"," +
    "\"sugar_dv\" : \"-\"," +
    "\"sugar_un\" : \"-\"," +
    "\"protein_a\" : \"13.5\"," +
    "\"protein_dv\" : \"18.0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"-\"," +
    "\"trans_fat_dv\" : \"-\"," +
    "\"trans_fat_un\" : \"-\"," +
    "\"sat_fat_a\" : \"-\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"-\"," +
    "\"mnuns_fat_a\" : \"-\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"-\"," +
    "\"pluns_fat_a\" : \"-\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"-\"," +
    "\"fibers_a\" : \"-\"," +
    "\"fibers_dv\" : \"-\"," +
    "\"fibers_un\" : \"-\"," +
    "\"sucrose_a\" : \"-\", " +
    "\"sucrose_dv\" : \"-\"," +
    "\"sucrose_un\" : \"-\"," +
    "\"cholesterol_a\" : \"-\"," +
    "\"cholesterol_dv\" : \"-\"," +
    "\"cholesterol_un\" : \"-\"," +
    "\"iron_a\" : \"0.1\"," +
    "\"iron_dv\" : \"1.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"-\"," +
    "\"zync_dv\" : \"-\"," +
    "\"zync_un\" : \"-\"," +
    "\"calcium_a\" : \"6.2\"," +
    "\"calcium_dv\" : \"1.0\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"-\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"-\"," +
    "\"vitamin_c_a\" : \"-\"," +
    "\"vitamin_c_dv\" : \"-\"," +
    "\"vitamin_c_un\" : \"-\"," +
    "\"vitamin_d_a\" : \"-\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"-\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"-\"," +
    "\"vitamin_b1_a\" : \"-\"," +
    "\"vitamin_b1_dv\" : \"-\"," +
    "\"vitamin_b1_un\" : \"-\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"-\"," +
    "\"vitamin_b6_a\" : \"-\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"-\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"-\"},{ " +
    "\"name\" : \"Batata doce crua\"," +
    "\"info_about\" : \"A batata doce é um dos vegetais mais nutritivos e versáteis que existe e pode ser encontrada facilmente durante todo o ano.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"118.5\", " +
    "\"energy_value_dv\" : \"5.9\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"28.2\"," +
    "\"carbohydrates_dv\" : \"9.4\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"4.2\"," +
    "\"sugar_dv\" : \"9.4\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"1.3\"," +
    "\"protein_dv\" : \"2.6\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0.1\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"2.6\"," +
    "\"fibers_dv\" : \"10.4\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.4\"," +
    "\"iron_dv\" : \"4.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.2\"," +
    "\"zync_dv\" : \"1.3\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"21.0\"," +
    "\"calcium_dv\" : \"2.6\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"14187.0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"2.4\"," +
    "\"vitamin_c_dv\" : \".0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"0\"," +
    "\"vitamin_b1_dv\" : \"0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.2\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Mandioca\"," +
    "\"info_about\" : \"As propriedades da mandioca podem ser explicadas pela sua excelente fonte de carboidratos, vitaminas do complexo B, vitamina C e minerais como magnésio, manganês, cobre, potássio. É fonte de fibras e isenta de glúten.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"151.0\", " +
    "\"energy_value_dv\" : \"7.5\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"36.2\"," +
    "\"carbohydrates_dv\" : \"12.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"1.4\"," +
    "\"sugar_dv\" : \"3.8\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"1.1\"," +
    "\"protein_dv\" : \"2.2\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0.1\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0.1\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"1.9\"," +
    "\"fibers_dv\" : \"7.6\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.3\"," +
    "\"iron_dv\" : \"3.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.2\"," +
    "\"zync_dv\" : \"1.3\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"15.0\"," +
    "\"calcium_dv\" : \"1.8\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"13.0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"20.6\"," +
    "\"vitamin_c_dv\" : \".0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"0\"," +
    "\"vitamin_b1_dv\" : \"0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Mel\"," +
    "\"info_about\" : \"Além de ser utilizado como adoçante natural, o mel também pode ser usado para fortalecer o sistema imunológico, melhorar a capacidade digestiva e até aliviar a prisão de ventre. Além disso, o mel é considerado anti-séptico, antioxidante, anti-reumático, diurético, digestivo, expectorante e calmante.\"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"309.0\", " +
    "\"energy_value_dv\" : \"15.5\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"84.0\"," +
    "\"carbohydrates_dv\" : \"28.0\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"82.0\"," +
    "\"sugar_dv\" : \"47.8\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"0.3\"," +
    "\"protein_dv\" : \"0\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"0\"," +
    "\"fibers_dv\" : \"0\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"0.3\"," +
    "\"iron_dv\" : \"3.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.2\"," +
    "\"zync_dv\" : \"1.3\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"10.0\"," +
    "\"calcium_dv\" : \"1.2\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"0.6\"," +
    "\"vitamin_c_dv\" : \".0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"0\"," +
    "\"vitamin_b1_dv\" : \"0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"" +
    "}," +
    "{ " +
    "\"name\" : \"Coco cru\"," +
    "\"info_about\" : \"Coco é uma fruta rica em nutrientes e pode trazer muitos benefícios à saúde. \"," +
    "\"standard_portion\" : \"100.0\"," +
    "\"energy_value_a\" : \"406.0\", " +
    "\"energy_value_dv\" : \"20.3\"," +
    "\"energy_value_un\" : \"kcal\"," +
    "\"carbohydrates_a\" : \"10.4\"," +
    "\"carbohydrates_dv\" : \"3.4\"," +
    "\"carbohydrates_un\" : \"g\"," +
    "\"sugar_a\" : \"0\"," +
    "\"sugar_dv\" : \"0\"," +
    "\"sugar_un\" : \"g\"," +
    "\"protein_a\" : \"3.7\"," +
    "\"protein_dv\" : \"7.4\"," +
    "\"protein_un\" : \"g\"," +
    "\"trans_fat_a\" : \"0\"," +
    "\"trans_fat_dv\" : \"0\"," +
    "\"trans_fat_un\" : \"g\"," +
    "\"sat_fat_a\" : \"0.1\"," +
    "\"sat_fat_dv\" : \"-\"," +
    "\"sat_fat_un\" : \"g\"," +
    "\"mnuns_fat_a\" : \"0.2\"," +
    "\"mnuns_fat_dv\" : \"-\"," +
    "\"mnuns_fat_un\" : \"g\"," +
    "\"pluns_fat_a\" : \"0.1\", " +
    "\"pluns_fat_dv\" : \"-\"," +
    "\"pluns_fat_un\" : \"g\"," +
    "\"fibers_a\" : \"5.4\"," +
    "\"fibers_dv\" : \"21.6\"," +
    "\"fibers_un\" : \"g\"," +
    "\"sucrose_a\" : \"0\", " +
    "\"sucrose_dv\" : \"0\"," +
    "\"sucrose_un\" : \"g\"," +
    "\"cholesterol_a\" : \"0\"," +
    "\"cholesterol_dv\" : \"0\"," +
    "\"cholesterol_un\" : \"mg\"," +
    "\"iron_a\" : \"1.8\"," +
    "\"iron_dv\" : \"18.0\"," +
    "\"iron_un\" : \"mg\"," +
    "\"zync_a\" : \"0.9\"," +
    "\"zync_dv\" : \"6.0\"," +
    "\"zync_un\" : \"mg\"," +
    "\"calcium_a\" : \"6.0\"," +
    "\"calcium_dv\" : \"0.7\"," +
    "\"calcium_un\" : \"mg\"," +
    "\"vitamin_a_a\" : \"12.0\"," +
    "\"vitamin_a_dv\" : \"-\"," +
    "\"vitamin_a_un\" : \"IU\"," +
    "\"vitamin_c_a\" : \"2.5\"," +
    "\"vitamin_c_dv\" : \"6.0\"," +
    "\"vitamin_c_un\" : \"mg\"," +
    "\"vitamin_d_a\" : \"0\"," +
    "\"vitamin_d_dv\" : \"-\"," +
    "\"vitamin_d_un\" : \"IU\"," +
    "\"vitamin_e_a\" : \"-\"," +
    "\"vitamin_e_dv\" : \"-\"," +
    "\"vitamin_e_un\" : \"mg\"," +
    "\"vitamin_b1_a\" : \"0,1\"," +
    "\"vitamin_b1_dv\" : \"7.0\"," +
    "\"vitamin_b1_un\" : \"mg\"," +
    "\"vitamin_b3_a\" : \"-\"," +
    "\"vitamin_b3_dv\" : \"-\"," +
    "\"vitamin_b3_un\" : \"mg\"," +
    "\"vitamin_b6_a\" : \"0.1\"," +
    "\"vitamin_b6_dv\" : \"-\"," +
    "\"vitamin_b6_un\" : \"mg\"," +
    "\"vitamin_b9_a\" : \"-\"," +
    "\"vitamin_b9_dv\" : \"-\"," +
    "\"vitamin_b9_un\" : \"-\"," +
    "\"vitamin_b12_a\" : \"-\"," +
    "\"vitamin_b12_dv\" : \"-\"," +
    "\"vitamin_b12_un\" : \"mg\"}]";

const styleOrange = TextStyle(
    color: Colors.deepOrange, fontWeight: FontWeight.bold, fontSize: 20.0);
const styleBlack = TextStyle(
    color: Colors.black, fontWeight: FontWeight.normal, fontSize: 18.0);

class DataSearch extends SearchDelegate<String> {
  List recentFoodData = [];

  final List<String> foodData;
  final List<Food> allFood;
  final List<Food> allFavorites;

  DataSearch(this.foodData, this.allFood, this.allFavorites);

  @override
  List<Widget> buildActions(BuildContext context) {
    // Ações pra AppBar
    return [
      IconButton(
          icon: Icon(Icons.clear),
          onPressed: () {
            query = "";
          })
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    //
    return IconButton(
        icon: AnimatedIcon(
            icon: AnimatedIcons.menu_arrow, progress: transitionAnimation),
        onPressed: () {
          close(context, null);
        });
  }

  @override
  Widget buildResults(BuildContext context) {
    // Mostrar resultados
    return null;
  }

  List<TextSpan> _getSpans(String text, String matchWord) {
    List<TextSpan> spans = [];
    int spanBoundary = 0;
    if (matchWord.isEmpty) {
      spans.add(TextSpan(text: text, style: styleBlack));
      return spans;
    }
    do {
      // look for the next match
      final startIndex = removeDiacritics(text)
          .toLowerCase()
          .indexOf(removeDiacritics(matchWord).toLowerCase(), spanBoundary);
      // if no more matches then add the rest of the string without style
      if (startIndex == -1) {
        spans.add(
            TextSpan(text: text.substring(spanBoundary), style: styleBlack));
        return spans;
      }
      // add any unstyled text before the next match
      if (startIndex > spanBoundary) {
        spans.add(TextSpan(
            text: text.substring(spanBoundary, startIndex), style: styleBlack));
      }
      // style the matched text
      final endIndex = startIndex + matchWord.length;
      final spanText = text.substring(startIndex, endIndex);
      spans.add(TextSpan(text: spanText, style: styleOrange));
      // mark the boundary to start the next search from
      spanBoundary = endIndex;
      // continue until there are no more matches
    } while (spanBoundary < text.length);
    return spans;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // Mostrar algo quando alguem pesquisar alguma coisa

    final suggestionList = query.isEmpty
        ? recentFoodData
        : foodData
            .where((String p) => removeDiacritics(p)
                .toLowerCase()
                .contains(removeDiacritics(query).toLowerCase()))
            .toList();

    return ListView.builder(
      itemBuilder: (context, index) => ListTile(
        onTap: () {
            recentFoodData.add(suggestionList[index]);
            recentFoodData.length > 4
                ? recentFoodData.removeLast()
                : print("not yet");
            Food matchingFood = (allFood
                      .firstWhere((f) => f.name == suggestionList[index]));
            matchingFood != null ?
            Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => itemViewScreen(matchingFood, allFavorites),
                )) : print("");
        },
        leading: Icon(
          Icons.fastfood,
          color: Colors.deepOrange,
        ),
        title: RichText(
            text: TextSpan(children: _getSpans(suggestionList[index], query))),
      ),
      itemCount: suggestionList.length,
    );
  }

  void _setUmaLetra(List<TextSpan> spans, String letter) {
    for (int n = 0; n < spans.length; n++) {
      if (spans[n].text.contains(letter)) {
        spans.removeAt(n);
        spans.insert(n, TextSpan(text: letter, style: styleOrange));
      }
    }
  }
}

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class itemViewScreen extends StatefulWidget {
  final Food food;
  final List<Food> favoriteFoods;

  itemViewScreen(this.food, this.favoriteFoods);

  @override
  _itemViewScreenState createState() => _itemViewScreenState();
}

class _itemViewScreenState extends State<itemViewScreen> {
  final List<String> listOfNutrientNames = [
    "Valor energético:",
    "Carbohidratos:",
    "Açúcares:",
    "Proteínas:",
    "Gordura Trans:",
    "Gordura Saturada:",
    "Gordura monoinsaturada:",
    "Gordura poliinsaturada:",
    "Fibras:",
    "Sacarose:",
    "Colesterol:",
    "Ferro:",
    "Zínco:",
    "Cálcio:",
    "Vitamina A:",
    "Vitamina C:",
    "Vitamina D:",
    "Vitamina E:",
    "Vitamina A:",
    "Vitamina B1:",
    "Vitamina B3:",
    "Vitamina B6:",
    "Vitamina B9:",
    "Vitamina B12:"
  ];

  final _servingController = new TextEditingController();

  double multiplier = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(
              "NewTrition",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 26.0,
                  fontWeight: FontWeight.bold),
            ),
            actions: <Widget>[
              IconButton(
                      onPressed: () {
                        widget.food.isFavorite =
                            !widget.food.isFavorite;
                        widget.food.isFavorite
                            ? widget.favoriteFoods.add(widget.food)
                            : widget.favoriteFoods.remove(widget.food);
                        setState(() {});
                      },
                      icon: Icon(
                        widget.food.isFavorite
                            ? Icons.favorite
                            : Icons.favorite_border,
                        color: Colors.deepOrange,
                      ),
                    )
            ],
            backgroundColor: Colors.orange,
            centerTitle: true,),
        body: SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Row(
                  children: [
                    ResponsiveContainer(
                      heightPercent: 86.4,
                      widthPercent: 100.0,
                      child: Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Container(
                          decoration: BoxDecoration(
                              color: Colors.orange,
                              boxShadow: [
                                BoxShadow(
                                    color: Colors.orangeAccent,
                                    blurRadius: 20.0,
                                    spreadRadius: 2.0)
                              ],
                              borderRadius: BorderRadius.circular(15.0),
                              shape: BoxShape.rectangle),
                          child: Stack(
                            children: <Widget>[
                              Column(
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      ResponsiveContainer(
                                        widthPercent: 97.0,
                                        heightPercent: 10.0,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              5.0, 10.0, 5.0, 0),
                                          child: Container(
                                            child: Card(
                                              elevation: 15.0,
                                              color: Colors.white70,
                                              margin: EdgeInsets.fromLTRB(
                                                  5.0, 3.0, 5.0, 2.0),
                                              child: Container(
                                                alignment: Alignment(0, 0),
                                                child: Padding(
                                                  padding: EdgeInsets.all(3.5),
                                                  child: AutoSizeText(
                                                    widget.food.name,
                                                    maxLines: 2,
                                                    textAlign: TextAlign.center,
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontStyle:
                                                            FontStyle.normal,
                                                        fontSize: 24.0),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      ResponsiveContainer(
                                        widthPercent: 97.0,
                                        heightPercent: 13.0,
                                        child: Padding(
                                          padding: const EdgeInsets.fromLTRB(
                                              5.0, 10.0, 5.0, 0),
                                          child: Container(
                                            child: Card(
                                              elevation: 15.0,
                                              color: Colors.white70,
                                              margin: EdgeInsets.fromLTRB(
                                                  5.0, 0.0, 5.0, 2.0),
                                              child: Container(
                                                alignment: Alignment(0, 0),
                                                child: Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      3.5, 3.5, 3.5, 0),
                                                  child: SingleChildScrollView(
                                                    child: AutoSizeText(
                                                      widget.food.info_about,
                                                      maxLines: 10,
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          fontSize: 13.0),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Column(children: <Widget>[
                                        ResponsiveContainer(
                                          widthPercent: 64.66666666666667,
                                          heightPercent: 6.7,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                5.0, 10.0, 5.0, 0),
                                            child: Container(
                                              child: Card(
                                                elevation: 15.0,
                                                color: Colors.white70,
                                                margin: EdgeInsets.fromLTRB(
                                                    5.0, 0.0, 5.0, 2.0),
                                                child: Container(
                                                  alignment: Alignment(0, 0),
                                                  child: Padding(
                                                    padding:
                                                        EdgeInsets.all(3.5),
                                                    child: AutoSizeText(
                                                      "Porção ",
                                                      textAlign:
                                                          TextAlign.center,
                                                      style: TextStyle(
                                                          color: Colors.black,
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          fontStyle:
                                                              FontStyle.normal,
                                                          fontSize: 24.0),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      ]),
                                      Column(
                                        children: <Widget>[
                                          ResponsiveContainer(
                                            widthPercent: 32.0,
                                            heightPercent: 6.7,
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.fromLTRB(
                                                      0.0, 10.0, 5.0, 0),
                                              child: Container(
                                                child: Card(
                                                  elevation: 15.0,
                                                  color: Colors.white70,
                                                  margin: EdgeInsets.fromLTRB(
                                                      0.0, 0.0, 5.0, 2.0),
                                                  child: Container(
                                                    child: Center(
                                                      child: AutoSizeText(
                                                        (double.parse(widget.food
                                                                        .standard_portion)
                                                                    .toStringAsFixed(
                                                                        1)
                                                                    .endsWith(
                                                                        "0")
                                                                ? double.parse(widget.food
                                                                        .standard_portion)
                                                                    .toStringAsFixed(
                                                                        0)
                                                                : double.parse(widget.food
                                                                        .standard_portion)
                                                                    .toStringAsFixed(
                                                                        1)) +
                                                            "g",
                                                        maxLines: 1,
                                                        textAlign:
                                                            TextAlign.center,
                                                        style: TextStyle(
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            fontStyle: FontStyle
                                                                .normal,
                                                            fontSize: 16.0),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                  SingleChildScrollView(
                                    child: Row(
                                      children: <Widget>[
                                        ResponsiveContainer(
                                          widthPercent: 97.0,
                                          heightPercent: 53.0,
                                          child: Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                5.0, 10.0, 5.0, 0),
                                            child: Container(
                                              child: Card(
                                                elevation: 15.0,
                                                color: Colors.white70,
                                                margin: EdgeInsets.fromLTRB(
                                                    5.0, 0.0, 5.0, 2.0),
                                                child: Container(
                                                  alignment: Alignment(0, -1),
                                                  child: Padding(
                                                    padding:
                                                        EdgeInsets.all(3.5),
                                                    child:
                                                        SingleChildScrollView(
                                                      child: Column(
                                                        children: <Widget>[
                                                          Visibility(
                                                              visible:
                                                                  widget.food.energy_value_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[0],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.energy_value_a + widget.food.energy_value_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.energy_value_dv + "%"),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.carbohydrates_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[1],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.carbohydrates_a + widget.food.carbohydrates_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.carbohydrates_dv + "%"),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.sugar_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[2],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.sugar_a + widget.food.sugar_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.sugar_dv + "%"),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.protein_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[3],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.protein_a + widget.food.protein_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.protein_dv + "%"),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.trans_fat_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[4],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.trans_fat_a + widget.food.trans_fat_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.trans_fat_dv + (widget.food.trans_fat_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.sat_fat_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[5],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.sat_fat_a + widget.food.sat_fat_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.sat_fat_dv + (widget.food.sat_fat_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.mnuns_fat_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[6],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 12.5),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.mnuns_fat_a + widget.food.mnuns_fat_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.mnuns_fat_dv + (widget.food.mnuns_fat_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.pluns_fat_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[7],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 13.5),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.pluns_fat_a + widget.food.pluns_fat_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 13.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.pluns_fat_dv + (widget.food.pluns_fat_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.fibers_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[8],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.fibers_a + widget.food.fibers_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.fibers_dv + (widget.food.fibers_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.sucrose_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[8],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.sucrose_a + widget.food.sucrose_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.sucrose_dv + (widget.food.sucrose_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.cholesterol_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[9],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.cholesterol_a + widget.food.cholesterol_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.cholesterol_dv + (widget.food.cholesterol_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.iron_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[10],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.iron_a + widget.food.iron_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.iron_dv + (widget.food.iron_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.zync_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[11],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.zync_a + widget.food.zync_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.zync_dv + (widget.food.zync_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.calcium_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[12],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.calcium_a + widget.food.calcium_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.calcium_dv + (widget.food.calcium_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.vitamin_a_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[13],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_a_a + widget.food.vitamin_a_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_a_dv + (widget.food.vitamin_a_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.vitamin_c_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[14],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_c_a + widget.food.vitamin_c_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_c_dv + (widget.food.vitamin_c_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.vitamin_d_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[15],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_d_a + widget.food.vitamin_d_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_d_dv + (widget.food.vitamin_d_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.vitamin_e_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[16],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_e_a + widget.food.vitamin_e_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_e_dv + (widget.food.vitamin_e_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.vitamin_b1_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[17],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_b1_a + widget.food.vitamin_b1_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_b1_dv + (widget.food.vitamin_b1_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.vitamin_b3_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[18],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_b3_a + widget.food.vitamin_b3_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_b3_dv + (widget.food.vitamin_b3_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.vitamin_b6_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[19],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_b6_a + widget.food.vitamin_b6_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_b6_dv + (widget.food.vitamin_b6_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.vitamin_b9_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[20],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_b9_a + widget.food.vitamin_b9_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_b9_dv + (widget.food.vitamin_b9_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              )),
                                                          Visibility(
                                                              visible:
                                                                  widget.food.vitamin_b12_a !=
                                                                          "-"
                                                                      ? true
                                                                      : false,
                                                              child:
                                                                  ResponsiveContainer(
                                                                widthPercent:
                                                                    100.0,
                                                                heightPercent:
                                                                    6.7,
                                                                child: Padding(
                                                                  padding:
                                                                      const EdgeInsets
                                                                              .fromLTRB(
                                                                          2.0,
                                                                          2.0,
                                                                          2.0,
                                                                          0),
                                                                  child:
                                                                      Container(
                                                                    child: Card(
                                                                      elevation:
                                                                          15.0,
                                                                      color: Colors
                                                                          .white,
                                                                      child:
                                                                          Container(
                                                                        alignment: Alignment(
                                                                            0,
                                                                            0),
                                                                        child:
                                                                            Padding(
                                                                          padding:
                                                                              EdgeInsets.all(0.5),
                                                                          child:
                                                                              Row(
                                                                            crossAxisAlignment:
                                                                                CrossAxisAlignment.stretch,
                                                                            children: <Widget>[
                                                                              ResponsiveContainer(
                                                                                heightPercent: 100.0,
                                                                                widthPercent: 42.0,
                                                                                child: Column(
                                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                                  children: <Widget>[
                                                                                    Center(
                                                                                      child: Padding(
                                                                                        padding: EdgeInsets.fromLTRB(3, 3, 3, 3),
                                                                                        child: AutoSizeText(
                                                                                          listOfNutrientNames[21],
                                                                                          textAlign: TextAlign.start,
                                                                                          style: TextStyle(color: Colors.orange[900], fontWeight: FontWeight.bold, fontSize: 16.0),
                                                                                        ),
                                                                                      ),
                                                                                    )
                                                                                  ],
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 25.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_b12_a + widget.food.vitamin_b12_un),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              ),
                                                                              Card(
                                                                                margin: EdgeInsets.fromLTRB(5.0, 2.0, 0, 2.0),
                                                                                color: Colors.black,
                                                                                child: ResponsiveContainer(
                                                                                  heightPercent: 100.0,
                                                                                  widthPercent: 15.0,
                                                                                  child: Padding(
                                                                                    padding: EdgeInsets.only(top: 4.0),
                                                                                    child: Center(
                                                                                      child: AutoSizeText(
                                                                                        (widget.food.vitamin_b12_dv + (widget.food.vitamin_b12_dv != "-" ? "%" : "")),
                                                                                        textAlign: TextAlign.end,
                                                                                        style: TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 15.0),
                                                                                      ),
                                                                                    ),
                                                                                  ),
                                                                                ),
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ))
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ]),
        ));
  }
}

class Food {
  int index = 0;
  bool isFavorite;
  String name,
      info_about,
      standard_portion,
      energy_value_a,
      energy_value_dv,
      energy_value_un,
      carbohydrates_a,
      carbohydrates_dv,
      carbohydrates_un,
      sugar_a,
      sugar_dv,
      sugar_un,
      protein_a,
      protein_dv,
      protein_un,
      trans_fat_a,
      trans_fat_dv,
      trans_fat_un,
      sat_fat_a,
      sat_fat_dv,
      sat_fat_un,
      mnuns_fat_a,
      mnuns_fat_dv,
      mnuns_fat_un,
      pluns_fat_a,
      pluns_fat_dv,
      pluns_fat_un,
      fibers_a,
      fibers_dv,
      fibers_un,
      sucrose_a,
      sucrose_dv,
      sucrose_un,
      cholesterol_a,
      cholesterol_dv,
      cholesterol_un,
      iron_a,
      iron_dv,
      iron_un,
      zync_a,
      zync_dv,
      zync_un,
      calcium_a,
      calcium_dv,
      calcium_un,
      vitamin_a_a,
      vitamin_a_dv,
      vitamin_a_un,
      vitamin_c_a,
      vitamin_c_dv,
      vitamin_c_un,
      vitamin_d_a,
      vitamin_d_dv,
      vitamin_d_un,
      vitamin_e_a,
      vitamin_e_dv,
      vitamin_e_un,
      vitamin_b1_a,
      vitamin_b1_dv,
      vitamin_b1_un,
      vitamin_b3_a,
      vitamin_b3_dv,
      vitamin_b3_un,
      vitamin_b6_a,
      vitamin_b6_dv,
      vitamin_b6_un,
      vitamin_b9_a,
      vitamin_b9_dv,
      vitamin_b9_un,
      vitamin_b12_a,
      vitamin_b12_dv,
      vitamin_b12_un;

  Food(
      this.index,
      this.isFavorite,
      this.name,
      this.info_about,
      this.standard_portion,
      this.energy_value_a,
      this.energy_value_dv,
      this.energy_value_un,
      this.carbohydrates_a,
      this.carbohydrates_dv,
      this.carbohydrates_un,
      this.sugar_a,
      this.sugar_dv,
      this.sugar_un,
      this.protein_a,
      this.protein_dv,
      this.protein_un,
      this.trans_fat_a,
      this.trans_fat_dv,
      this.trans_fat_un,
      this.sat_fat_a,
      this.sat_fat_dv,
      this.sat_fat_un,
      this.mnuns_fat_a,
      this.mnuns_fat_dv,
      this.mnuns_fat_un,
      this.pluns_fat_a,
      this.pluns_fat_dv,
      this.pluns_fat_un,
      this.fibers_a,
      this.fibers_dv,
      this.fibers_un,
      this.sucrose_a,
      this.sucrose_dv,
      this.sucrose_un,
      this.cholesterol_a,
      this.cholesterol_dv,
      this.cholesterol_un,
      this.iron_a,
      this.iron_dv,
      this.iron_un,
      this.zync_a,
      this.zync_dv,
      this.zync_un,
      this.calcium_a,
      this.calcium_dv,
      this.calcium_un,
      this.vitamin_a_a,
      this.vitamin_a_dv,
      this.vitamin_a_un,
      this.vitamin_c_a,
      this.vitamin_c_dv,
      this.vitamin_c_un,
      this.vitamin_d_a,
      this.vitamin_d_dv,
      this.vitamin_d_un,
      this.vitamin_e_a,
      this.vitamin_e_dv,
      this.vitamin_e_un,
      this.vitamin_b1_a,
      this.vitamin_b1_dv,
      this.vitamin_b1_un,
      this.vitamin_b3_a,
      this.vitamin_b3_dv,
      this.vitamin_b3_un,
      this.vitamin_b6_a,
      this.vitamin_b6_dv,
      this.vitamin_b6_un,
      this.vitamin_b9_a,
      this.vitamin_b9_dv,
      this.vitamin_b9_un,
      this.vitamin_b12_a,
      this.vitamin_b12_dv,
      this.vitamin_b12_un);

  Food.fromJson(Map<String, dynamic> data, int index, bool isFavorite) {
    this.index = index;
    this.isFavorite = isFavorite;
    this.name = data["name"];
    this.info_about = data["info_about"];
    this.standard_portion = data["standard_portion"];
    this.energy_value_a = data["energy_value_a"];
    this.energy_value_dv = data["energy_value_dv"];
    this.energy_value_un = data["energy_value_un"];
    this.carbohydrates_a = data["carbohydrates_a"];
    this.carbohydrates_dv = data["carbohydrates_dv"];
    this.carbohydrates_un = data["carbohydrates_un"];
    this.sugar_a = data["sugar_a"];
    this.sugar_dv = data["sugar_dv"];
    this.sugar_un = data["sugar_un"];
    this.protein_a = data["protein_a"];
    this.protein_dv = data["protein_dv"];
    this.protein_un = data["protein_un"];
    this.trans_fat_a = data["trans_fat_a"];
    this.trans_fat_dv = data["trans_fat_dv"];
    this.trans_fat_un = data["trans_fat_un"];
    this.sat_fat_a = data["sat_fat_a"];
    this.sat_fat_dv = data["sat_fat_dv"];
    this.sat_fat_un = data["sat_fat_un"];
    this.mnuns_fat_a = data["mnuns_fat_a"];
    this.mnuns_fat_dv = data["mnuns_fat_dv"];
    this.mnuns_fat_un = data["mnuns_fat_un"];
    this.pluns_fat_a = data["pluns_fat_a"];
    this.pluns_fat_dv = data["pluns_fat_dv"];
    this.pluns_fat_un = data["pluns_fat_un"];
    this.fibers_a = data["fibers_a"];
    this.fibers_dv = data["fibers_dv"];
    this.fibers_un = data["fibers_un"];
    this.sucrose_a = data["sucrose_a"];
    this.sucrose_dv = data["sucrose_dv"];
    this.sucrose_un = data["sucrose_un"];
    this.cholesterol_a = data["cholesterol_a"];
    this.cholesterol_dv = data["cholesterol_dv"];
    this.cholesterol_un = data["cholesterol_un"];
    this.iron_a = data["iron_a"];
    this.iron_dv = data["iron_dv"];
    this.iron_un = data["iron_un"];
    this.zync_a = data["zync_a"];
    this.zync_dv = data["zync_dv"];
    this.zync_un = data["zync_un"];
    this.calcium_a = data["calcium_a"];
    this.calcium_dv = data["calcium_dv"];
    this.calcium_un = data["calcium_un"];
    this.vitamin_a_a = data["vitamin_a_a"];
    this.vitamin_a_dv = data["vitamin_a_dv"];
    this.vitamin_a_un = data["vitamin_a_un"];
    this.vitamin_c_a = data["vitamin_c_a"];
    this.vitamin_c_dv = data["vitamin_c_dv"];
    this.vitamin_c_un = data["vitamin_c_un"];
    this.vitamin_d_a = data["vitamin_d_a"];
    this.vitamin_d_dv = data["vitamin_d_dv"];
    this.vitamin_d_un = data["vitamin_d_un"];
    this.vitamin_e_a = data["vitamin_e_a"];
    this.vitamin_e_dv = data["vitamin_e_dv"];
    this.vitamin_e_un = data["vitamin_e_un"];
    this.vitamin_b1_a = data["vitamin_b1_a"];
    this.vitamin_b1_dv = data["vitamin_b1_dv"];
    this.vitamin_b1_un = data["vitamin_b1_un"];
    this.vitamin_b3_a = data["vitamin_b3_a"];
    this.vitamin_b3_dv = data["vitamin_b3_dv"];
    this.vitamin_b3_un = data["vitamin_b3_un"];
    this.vitamin_b6_a = data["vitamin_b6_a"];
    this.vitamin_b6_dv = data["vitamin_b6_dv"];
    this.vitamin_b6_un = data["vitamin_b6_un"];
    this.vitamin_b9_a = data["vitamin_b9_a"];
    this.vitamin_b9_dv = data["vitamin_b9_dv"];
    this.vitamin_b9_un = data["vitamin_b9_un"];
    this.vitamin_b12_a = data["vitamin_b12_a"];
    this.vitamin_b12_dv = data["vitamin_b12_dv"];
    this.vitamin_b12_un = data["vitamin_b12_un"];
  }
}

class _HomeState extends State<Home> {
  List<Food> foodList, favoriteFoodList;
  List<String> stringFoodNameList;
  int bottomNavigationBarIndex = 0;

  @override
  initState() {
    super.initState();
    foodList = new List();
    favoriteFoodList = new List();
    stringFoodNameList = new List();
    _readData().then((data) 
    {
      setState(() {
        favoriteFoodList = json.decode(data);
      });
    });
    var jsonDataList = jsonDecode(jsonData);
    var jsonDataList2 = jsonDecode(jsonData2);
    int i = 0;
    bool stateOfFave = false;
    for (var obj in jsonDataList) {
      Food food = new Food.fromJson(obj, i, stateOfFave);
      var chk = favoriteFoodList.firstWhere((o) => o.name == food.name, orElse: () => null);
      chk == null ? print("nothingDone") : food.isFavorite = chk.isFavorite;
      i++;
      foodList.add(food);
      stringFoodNameList.add((food.name));
    }
    for (var obj in jsonDataList2) {
      Food food = new Food.fromJson(obj, i, stateOfFave);
      var chk = favoriteFoodList.firstWhere((o) => o.name == food.name, orElse: () => null);
      chk == null ? print("nothingDone") : food.isFavorite = chk.isFavorite;
      i++;
      foodList.add(food);
      stringFoodNameList.add((food.name));
    }
    foodList.sort((a, b) => a.name.compareTo(b.name));
  }

  Future<File> _getFile() async {
    final directory = await getApplicationDocumentsDirectory(); // lê o arquivo
    return File("${directory.path}/data.json");
  }

  Future<File> _saveData(List<Food> dataList) async // Salva dados novos a partir do arquivo
  {
    String data = json.encode(dataList);
    final file = await _getFile();
    return file.writeAsString(data);
  }

  Future<String> _readData() async // Lê os dados e utiliza no programa
  {
    try {
      final file = await _getFile();
      return file.readAsString();
    } catch (e) {
      return e.toString();
    }
  }

  /*@override
  void initState() {
    super.initState();

    _readData().then((data) {
      setState(() {
        favoriteFoodList = json.decode(data);
      });
    });
  }*/

  /*void _addToDo() {
    setState(() {
      Map<String, dynamic> newToDo = Map();
      newToDo["title"] = _toDoController.text;
      _toDoController.text = "";
      newToDo["ok"] = false;
      _toDoList.add(newToDo);

      _saveData();
    });
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text(
              "NewTrition",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 26.0,
                  fontWeight: FontWeight.bold),
            ),
            backgroundColor: Colors.orange,
            centerTitle: true,
            actions: <Widget>[
              IconButton(
                  icon: Icon(Icons.search),
                  onPressed: () {
                    showSearch(
                        context: context,
                        delegate: DataSearch(stringFoodNameList, foodList, favoriteFoodList),
                    );}
              )],),
        body: bottomNavigationBarIndex == 0
            ? ListView.builder(
                // CONSTRUÇÃO DO MENU PRINCIPAL
                itemCount: foodList.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    subtitle: Align(
                      alignment: Alignment(0.9, 0.25),
                      child: Text((double.parse(foodList[index].energy_value_a)
                                  .toStringAsFixed(1)
                                  .endsWith("0")
                              ? double.parse(foodList[index].energy_value_a)
                                  .toStringAsFixed(0)
                              : double.parse(foodList[index].energy_value_a)
                                  .toStringAsFixed(1)) +
                          foodList[index].energy_value_un +
                          "/" +
                          double.parse(foodList[index].standard_portion)
                              .toStringAsFixed(0) +
                          "gr"),
                    ),
                    title: Text(foodList[index].name),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) =>
                                itemViewScreen(foodList[index],favoriteFoodList),
                          ));
                    },
                    leading: IconButton(
                      onPressed: () {
                        foodList[index].isFavorite =
                            !foodList[index].isFavorite;
                        foodList[index].isFavorite
                            ? favoriteFoodList.add(foodList[index])
                            : favoriteFoodList.remove(foodList[index]);
                        _saveData(favoriteFoodList);
                        setState(() {});
                      },
                      icon: Icon(
                        foodList[index].isFavorite
                            ? Icons.favorite
                            : Icons.favorite_border,
                        color: Colors.deepOrange,
                      ),
                    ),
                  );
                },
              )
            : favoriteFoodList.isNotEmpty
                ? ListView.builder(
                    // CONSTRUÇÃO DO MENU DE FAVORITOS
                    itemCount: favoriteFoodList.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ListTile(
                        subtitle: Align(
                          alignment: Alignment(0.9, 0.25),
                          child: Text((double.parse(favoriteFoodList[index]
                                          .energy_value_a)
                                      .toStringAsFixed(1)
                                      .endsWith("0")
                                  ? double.parse(favoriteFoodList[index]
                                          .energy_value_a)
                                      .toStringAsFixed(0)
                                  : double.parse(favoriteFoodList[index]
                                          .energy_value_a)
                                      .toStringAsFixed(1)) +
                              foodList[index].energy_value_un +
                              "/" +
                              double.parse(
                                      favoriteFoodList[index].standard_portion)
                                  .toStringAsFixed(0) +
                              "gr"),
                        ),
                        title: Text(favoriteFoodList[index].name),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    itemViewScreen(favoriteFoodList[index],favoriteFoodList)
                                    )
                          );
                        },
                        leading: IconButton(
                          onPressed: () {
                            setState(() {
                              var aux = foodList.firstWhere((f) =>
                                  f.name == favoriteFoodList[index].name);
                              print(aux.toString());
                              aux.isFavorite = !aux.isFavorite;
                              favoriteFoodList.remove(favoriteFoodList[index]);
                              _saveData(favoriteFoodList);
                            });
                          },
                          icon: Icon(
                            Icons.favorite,
                            color: Colors.deepOrange,
                          ),
                        ),
                      );
                    },
                  )
                : Center(
                    child: Text(
                    "Você ainda não armazenou seus alimentos favoritos"
                    "\n\n\nAssim que adicionar eles aparecerão aqui.",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        color: Colors.orange,
                        fontSize: 25.0),
                  )),
        bottomNavigationBar: BottomNavigationBar(
            onTap: (index) {
              setState(() {
                bottomNavigationBarIndex = index;
                _saveData(favoriteFoodList);
              });
            },
            backgroundColor: Colors.orange,
            unselectedItemColor: Colors.orangeAccent,
            selectedItemColor: Colors.white,
            selectedLabelStyle:
                TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
            currentIndex: bottomNavigationBarIndex,
            items: [
              BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                  size: 30.0,
                ),
                title: Text(
                  "Principal",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.star, size: 30.0),
                title: Text(
                  "Favoritos",
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              )
            ]));
  }
}
